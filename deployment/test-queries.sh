curl --location --request POST '127.0.0.1:8080/create-order' \
--header 'Content-Type: application/json' \
--data-raw '{
    "user_id": 1,
    "products": [
        {
            "item_id": 2,
            "quantity": 1
        },
        {
            "item_id": 3,
            "quantity": 1
        }
    ]
}'

curl --location --request POST '127.0.0.1:8080/create-order' \
--header 'Content-Type: application/json' \
--data-raw '{
    "user_id": 1,
    "products": [
        {
            "item_id": 7,
            "quantity": 1
        },
        {
            "item_id": 3,
            "quantity": 1
        }
    ]
}'

curl --location --request POST '127.0.0.1:8080/create-order' \
--header 'Content-Type: application/json' \
--data-raw '{
    "user_id": 1,
    "products": [
        {
            "item_id": 1,
            "quantity": 3
        },
        {
            "item_id": 2,
            "quantity": 3
        }
    ]
}'


curl --location --request POST '127.0.0.1:8080/create-order' \
--header 'Content-Type: application/json' \
--data-raw '{
    "user_id": 1,
    "products": [
        {
            "item_id": 1,
            "quantity": 300
        },
        {
            "item_id": 2,
            "quantity": 1000
        }
    ]
}'
