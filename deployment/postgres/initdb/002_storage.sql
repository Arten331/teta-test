SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE IF EXISTS storage;

create user storage with encrypted password 'storage';

CREATE DATABASE "storage" WITH OWNER "storage" TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.utf8';



--
-- Name: storage; Type: DATABASE; Schema: -; Owner: registry
--

grant all privileges on database storage to registry;

\connect storage

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: storage; Type: SCHEMA; Schema: -; Owner: registry
--

CREATE SCHEMA storage;


ALTER SCHEMA storage OWNER TO storage;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: item_balance; Type: TABLE; Schema: storage; Owner: registry
--

CREATE TABLE storage.item_balance
(
    id         integer NOT NULL,
    product_id integer NOT NULL,
    quantity   integer NOT NULL
);

ALTER TABLE storage.item_balance
    ADD
        CONSTRAINT positive_quantity CHECK (item_balance.quantity >= 0);


ALTER TABLE storage.item_balance
    OWNER TO registry;

--
-- Name: item_balance_id_seq; Type: SEQUENCE; Schema: storage; Owner: registry
--

CREATE SEQUENCE storage.item_balance_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE storage.item_balance_id_seq
    OWNER TO registry;

--
-- Name: item_balance_id_seq; Type: SEQUENCE OWNED BY; Schema: storage; Owner: registry
--

ALTER SEQUENCE storage.item_balance_id_seq OWNED BY storage.item_balance.id;


--
-- Name: item_balance_log; Type: TABLE; Schema: storage; Owner: registry
--

CREATE TABLE storage.item_balance_log
(
    id         integer           NOT NULL,
    product_id integer           NOT NULL,
    order_id   integer,
    operation  character varying NOT NULL,
    quantity   integer           NOT NULL
);


ALTER TABLE storage.item_balance_log
    OWNER TO registry;

--
-- Name: item_balance_log_id_seq; Type: SEQUENCE; Schema: storage; Owner: registry
--

CREATE SEQUENCE storage.item_balance_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE storage.item_balance_log_id_seq
    OWNER TO registry;

--
-- Name: item_balance_log_id_seq; Type: SEQUENCE OWNED BY; Schema: storage; Owner: registry
--

ALTER SEQUENCE storage.item_balance_log_id_seq OWNED BY storage.item_balance_log.id;


--
-- Name: item_balance id; Type: DEFAULT; Schema: storage; Owner: registry
--

ALTER TABLE ONLY storage.item_balance
    ALTER COLUMN id SET DEFAULT nextval('storage.item_balance_id_seq'::regclass);


--
-- Name: item_balance_log id; Type: DEFAULT; Schema: storage; Owner: registry
--

ALTER TABLE ONLY storage.item_balance_log
    ALTER COLUMN id SET DEFAULT nextval('storage.item_balance_log_id_seq'::regclass);


--
-- Data for Name: item_balance; Type: TABLE DATA; Schema: storage; Owner: registry
--

INSERT INTO storage.item_balance
VALUES (1, 1, 10);
INSERT INTO storage.item_balance
VALUES (2, 2, 10);
INSERT INTO storage.item_balance
VALUES (3, 3, 10);
INSERT INTO storage.item_balance
VALUES (4, 4, 10);
INSERT INTO storage.item_balance
VALUES (5, 5, 10);


--
-- Data for Name: item_balance_log; Type: TABLE DATA; Schema: storage; Owner: registry
--

INSERT INTO storage.item_balance_log
VALUES (1, 1, NULL, 'shipment', 10);
INSERT INTO storage.item_balance_log
VALUES (2, 2, NULL, 'shipment', 10);
INSERT INTO storage.item_balance_log
VALUES (3, 3, NULL, 'shipment', 10);
INSERT INTO storage.item_balance_log
VALUES (4, 4, NULL, 'shipment', 10);
INSERT INTO storage.item_balance_log
VALUES (5, 5, NULL, 'shipment', 10);


--
-- Name: item_balance_id_seq; Type: SEQUENCE SET; Schema: storage; Owner: registry
--

SELECT pg_catalog.setval('storage.item_balance_id_seq', 5, true);


--
-- Name: item_balance_log_id_seq; Type: SEQUENCE SET; Schema: storage; Owner: registry
--

SELECT pg_catalog.setval('storage.item_balance_log_id_seq', 5, true);


--
-- Name: item_balance_log item_balance_log_pk; Type: CONSTRAINT; Schema: storage; Owner: registry
--

ALTER TABLE ONLY storage.item_balance_log
    ADD CONSTRAINT item_balance_log_pk PRIMARY KEY (id);


--
-- Name: item_balance_id_uindex; Type: INDEX; Schema: storage; Owner: registry
--

CREATE UNIQUE INDEX item_balance_id_uindex ON storage.item_balance USING btree (id);


--
-- Name: item_balance_product_id_index; Type: INDEX; Schema: storage; Owner: registry
--

CREATE INDEX item_balance_product_id_index ON storage.item_balance USING btree (product_id);


--
-- PostgreSQL database dump complete
--

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA storage TO "storage";
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA storage TO "storage";
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA storage TO "storage";
