SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

ALTER DATABASE registry OWNER TO registry;

\connect registry

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: shop; Type: SCHEMA; Schema: -; Owner: registry
--

CREATE SCHEMA shop;


ALTER SCHEMA shop OWNER TO registry;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: item; Type: TABLE; Schema: shop; Owner: registry
--

CREATE TABLE shop.item
(
    id    integer           NOT NULL,
    name  character varying NOT NULL,
    price integer DEFAULT 0 NOT NULL
);


ALTER TABLE shop.item OWNER TO registry;

--
-- Name: items_id_seq; Type: SEQUENCE; Schema: shop; Owner: registry
--

CREATE SEQUENCE shop.items_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE CACHE 1;


ALTER TABLE shop.items_id_seq OWNER TO registry;

--
-- Name: items_id_seq; Type: SEQUENCE OWNED BY; Schema: shop; Owner: registry
--

ALTER SEQUENCE shop.items_id_seq OWNED BY shop.item.id;


--
-- Name: order; Type: TABLE; Schema: shop; Owner: registry
--

CREATE TABLE shop."order"
(
    id      integer NOT NULL,
    user_id integer,
    status  varchar
);


ALTER TABLE shop."order" OWNER TO registry;

--
-- Name: order_id_seq; Type: SEQUENCE; Schema: shop; Owner: registry
--

CREATE SEQUENCE shop.order_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE CACHE 1;


ALTER TABLE shop.order_id_seq OWNER TO registry;

--
-- Name: order_id_seq; Type: SEQUENCE OWNED BY; Schema: shop; Owner: registry
--

ALTER SEQUENCE shop.order_id_seq OWNED BY shop."order".id;


--
-- Name: purchase_item; Type: TABLE; Schema: shop; Owner: registry
--

CREATE TABLE shop.purchase_item
(
    id       integer NOT NULL,
    order_id integer NOT NULL,
    item_id  integer,
    quantity integer NOT NULL,
    price    integer
);


ALTER TABLE shop.purchase_item OWNER TO registry;

--
-- Name: purchase_item_id_seq; Type: SEQUENCE; Schema: shop; Owner: registry
--

CREATE SEQUENCE shop.purchase_item_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE CACHE 1;


ALTER TABLE shop.purchase_item_id_seq OWNER TO registry;

--
-- Name: purchase_item_id_seq; Type: SEQUENCE OWNED BY; Schema: shop; Owner: registry
--

ALTER SEQUENCE shop.purchase_item_id_seq OWNED BY shop.purchase_item.id;


--
-- Name: user; Type: TABLE; Schema: shop; Owner: registry
--

CREATE TABLE shop."user"
(
    id    integer           NOT NULL,
    email character varying NOT NULL
);


ALTER TABLE shop."user" OWNER TO registry;

--
-- Name: user_id_seq; Type: SEQUENCE; Schema: shop; Owner: registry
--

CREATE SEQUENCE shop.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE CACHE 1;


ALTER TABLE shop.user_id_seq OWNER TO registry;

--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: shop; Owner: registry
--

ALTER SEQUENCE shop.user_id_seq OWNED BY shop."user".id;


--
-- Name: item id; Type: DEFAULT; Schema: shop; Owner: registry
--

ALTER TABLE ONLY shop.item ALTER COLUMN id SET DEFAULT nextval('shop.items_id_seq'::regclass);


--
-- Name: order id; Type: DEFAULT; Schema: shop; Owner: registry
--

ALTER TABLE ONLY shop."order" ALTER COLUMN id SET DEFAULT nextval('shop.order_id_seq'::regclass);


--
-- Name: purchase_item id; Type: DEFAULT; Schema: shop; Owner: registry
--

ALTER TABLE ONLY shop.purchase_item ALTER COLUMN id SET DEFAULT nextval('shop.purchase_item_id_seq'::regclass);


--
-- Name: user id; Type: DEFAULT; Schema: shop; Owner: registry
--

ALTER TABLE ONLY shop."user" ALTER COLUMN id SET DEFAULT nextval('shop.user_id_seq'::regclass);


--
-- Data for Name: item; Type: TABLE DATA; Schema: shop; Owner: registry
--

COPY shop.item (id, name, price) FROM stdin;
1	Товар А	1
2	Товар Б	2
3	Товар В	3
4	Товар Г	4
5	Товар Д	5
\.


--
-- Data for Name: order; Type: TABLE DATA; Schema: shop; Owner: registry
--
insert into shop."order" (id, user_id, status)
select 1, 1, 'created';

--
-- Data for Name: purchase_item; Type: TABLE DATA; Schema: shop; Owner: registry
--

COPY shop.purchase_item (id, order_id, item_id, quantity, price) FROM stdin;
1	1	2	3	9
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: shop; Owner: registry
--

COPY shop."user" (id, email) FROM stdin;
1	first@email.ru
2	second@email.ru
3	third@email.ru
4	fourth@email.ru
\.


--
-- Name: items_id_seq; Type: SEQUENCE SET; Schema: shop; Owner: registry
--

SELECT pg_catalog.setval('shop.items_id_seq', 5, true);


--
-- Name: order_id_seq; Type: SEQUENCE SET; Schema: shop; Owner: registry
--

SELECT pg_catalog.setval('shop.order_id_seq', 6, true);


--
-- Name: purchase_item_id_seq; Type: SEQUENCE SET; Schema: shop; Owner: registry
--

SELECT pg_catalog.setval('shop.purchase_item_id_seq', 1, true);


--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: shop; Owner: registry
--

SELECT pg_catalog.setval('shop.user_id_seq', 4, true);


--
-- Name: item items_pk; Type: CONSTRAINT; Schema: shop; Owner: registry
--

ALTER TABLE ONLY shop.item
    ADD CONSTRAINT items_pk PRIMARY KEY (id);


--
-- Name: order order_pk; Type: CONSTRAINT; Schema: shop; Owner: registry
--

ALTER TABLE ONLY shop."order"
    ADD CONSTRAINT order_pk PRIMARY KEY (id);


--
-- Name: purchase_item purchase_item_pk; Type: CONSTRAINT; Schema: shop; Owner: registry
--

ALTER TABLE ONLY shop.purchase_item
    ADD CONSTRAINT purchase_item_pk PRIMARY KEY (id);


--
-- Name: user user_pk; Type: CONSTRAINT; Schema: shop; Owner: registry
--

ALTER TABLE ONLY shop."user"
    ADD CONSTRAINT user_pk PRIMARY KEY (id);


--
-- Name: user_email_uindex; Type: INDEX; Schema: shop; Owner: registry
--

CREATE UNIQUE INDEX user_email_uindex ON shop."user" USING btree (email);


--
-- Name: purchase_item purchase_item_item_id_fk; Type: FK CONSTRAINT; Schema: shop; Owner: registry
--

ALTER TABLE ONLY shop.purchase_item
    ADD CONSTRAINT purchase_item_item_id_fk FOREIGN KEY (item_id) REFERENCES shop.item(id);


--
-- PostgreSQL database dump complete
--
