SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE IF EXISTS wallet;

create user wallet with encrypted password 'wallet';

CREATE DATABASE wallet WITH OWNER wallet TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.utf8';

ALTER DATABASE wallet OWNER TO wallet;

\connect wallet

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

CREATE SCHEMA wallet;


ALTER SCHEMA wallet OWNER TO wallet;

SET default_tablespace = '';

SET default_table_access_method = heap;

CREATE TABLE wallet.account_balance (
                                        id integer NOT NULL,
                                        user_id integer NOT NULL,
                                        points integer NOT NULL
);

ALTER TABLE wallet.account_balance OWNER TO wallet;

CREATE SEQUENCE wallet.account_balance_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE wallet.account_balance_id_seq OWNER TO wallet;

ALTER SEQUENCE wallet.account_balance_id_seq OWNED BY wallet.account_balance.id;


CREATE TABLE wallet.account_balance_log (
                                            id integer NOT NULL,
                                            account_id integer NOT NULL,
                                            operation character varying NOT NULL,
                                            income integer NOT NULL
);


ALTER TABLE wallet.account_balance_log OWNER TO wallet;

CREATE SEQUENCE wallet.account_balance_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE wallet.account_balance_log_id_seq OWNER TO wallet;

ALTER SEQUENCE wallet.account_balance_log_id_seq OWNED BY wallet.account_balance_log.id;

ALTER TABLE ONLY wallet.account_balance ALTER COLUMN id SET DEFAULT nextval('wallet.account_balance_id_seq'::regclass);
ALTER TABLE ONLY wallet.account_balance_log ALTER COLUMN id SET DEFAULT nextval('wallet.account_balance_log_id_seq'::regclass);

INSERT INTO wallet.account_balance VALUES (1, 1, 100);
INSERT INTO wallet.account_balance VALUES (2, 2, 100);
INSERT INTO wallet.account_balance VALUES (3, 3, 100);
INSERT INTO wallet.account_balance VALUES (4, 4, 100);
INSERT INTO wallet.account_balance VALUES (5, 5, 100);

INSERT INTO wallet.account_balance_log VALUES (1, 1, 'income', 100);
INSERT INTO wallet.account_balance_log VALUES (2, 2, 'income', 100);
INSERT INTO wallet.account_balance_log VALUES (3, 3, 'income', 100);
INSERT INTO wallet.account_balance_log VALUES (4, 4, 'income', 100);
INSERT INTO wallet.account_balance_log VALUES (5, 5, 'income', 100);


SELECT pg_catalog.setval('wallet.account_balance_id_seq', 5, true);
SELECT pg_catalog.setval('wallet.account_balance_log_id_seq', 5, true);

GRANT ALL ON TABLE wallet.account_balance TO wallet;
GRANT ALL ON SEQUENCE wallet.account_balance_id_seq TO wallet;
GRANT ALL ON TABLE wallet.account_balance_log TO wallet;
GRANT ALL ON SEQUENCE wallet.account_balance_log_id_seq TO wallet;


GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA wallet TO "wallet";
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA wallet TO "wallet";
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA wallet TO "wallet";

-- grant all privileges on database wallet to registry;
