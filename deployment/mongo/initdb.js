print('Start MongoInit.js');
db.createUser(
    {
        user: "root",
        pwd: "pswd",
        roles: [
            {role: "userAdminAnyDatabase", db: "admin"},
            {role: "readWriteAnyDatabase", db: "admin"}]
    }
)

db = db.getSiblingDB('users');
db.createCollection('users');

db.createUser(
    {
        user: "users",
        pwd: "users",
        roles: [
            {
                role: "readWrite",
                db: "users"
            }
        ]
    }
);

db.users.insert({id: 1, email: "abc@mail.ru", last_order_created: new Date("2000-01-01T00:00:00")})
db.users.insert({id: 2, email: "google@mail.ru", last_order_created: new Date("2000-01-01T00:00:00")})
db.users.insert({id: 3, email: "mail@mail.ru", last_order_created: new Date("2000-01-01T00:00:00")})
db.users.insert({id: 4, email: "yandex@mail.ru", last_order_created: new Date("2000-01-01T00:00:00")})
db.users.insert({id: 5, email: "bank@mail.ru", last_order_created: new Date("2000-01-01T00:00:00")})
