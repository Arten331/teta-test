rs.initiate({
    _id: 'rs0',
    members:
        [
            {_id: 0, host: "mongo1:30001"},
            {_id: 1, host: "mongo2:30002"},
            {_id: 2, host: "mongo3:30003"},
        ],
})
;

rs.conf();
