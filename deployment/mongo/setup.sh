#!/usr/bin/env bash
while true; do
    echo "rs.status().ok" | mongo mongodb://mongo1:30001 --quiet &&
    echo "rs.status().ok" | mongo mongodb://mongo2:30002 --quiet &&
    echo "rs.status().ok" | mongo mongodb://mongo3:30003 --quiet
    if [[ 0 == $? ]]; then
        break
    fi
    echo 'Wait for all mongo replica ready...'
    sleep 1
done
cn=$(echo "rs.status().codeName" | mongo mongodb://mongo1:30001 --quiet)
if [[ "$cn" == "NotYetInitialized" ]]; then
    mongo mongodb://mongo1:30001 <replicaset.js
    sleep 60
    mongo mongodb://mongo1:30001 <initdb.js
fi
