start:
	docker-compose up -d
	sh ./deployment/test-queries.sh > /dev/null
	echo "waiting kafka re-balance 4 minutes"
	sleep 240
	sh ./deployment/test-queries.sh

create_swagger_readme:
	swag init -g swagger/http/api.go --dir internal/orders --output internal/orders/docs
	swagger2markup convert -i ./internal/orders/docs/swagger.yaml -d ./internal/orders/docs
	cat ./internal/orders/docs/overview.adoc ./internal/orders/docs/paths.adoc ./internal/orders/docs/definitions.adoc ./internal/orders/docs/security.adoc > ./internal/orders/docs/readme_swag.adoc
	rm  ./internal/orders/docs/overview.adoc ./internal/orders/docs/security.adoc ./internal/orders/docs/paths.adoc ./internal/orders/docs/definitions.adoc

lint:
	gofmt -s -w .
	golangci-lint run ./... --fix
