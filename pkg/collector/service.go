package collector

import (
	"context"

	"shop/pkg/logger"
	"shop/pkg/workpool"
)

type CommandCollector struct {
	worker          Worker
	collectors      []Collector
	commandsChannel chan ProcessableCommand // buffer for commands
}

type Worker interface {
	Submit(command workpool.ProcessableCommand)
}

type ProcessableCommand interface {
	Process() error
}

type Collector interface {
	Collect(ctx context.Context, ch chan ProcessableCommand)
}

type CommandCollectorConfiguration func(os *CommandCollector) error

func NewCommandCollector(cfgs ...CommandCollectorConfiguration) (CommandCollector, error) {
	os := CommandCollector{}
	// Apply all Configurations passed in
	for _, cfg := range cfgs {
		err := cfg(&os)
		if err != nil {
			return os, err
		}
	}

	os.commandsChannel = make(chan ProcessableCommand, 300)

	return os, nil
}

func (s *CommandCollector) Run(ctx context.Context, _ context.CancelFunc) {
	logger.L().Info("Start queue collector service")

	// Collect commands
	go func() {
		for _, collector := range s.collectors {
			collector.Collect(ctx, s.commandsChannel)
		}
	}()

	// Processing collected commands
	go func() {
		for {
			select {
			case processEvent := <-s.commandsChannel:
				s.worker.Submit(processEvent)
			case <-ctx.Done():
				logger.L().Info("Stop queue collector service")
				return
			}
		}
	}()
}

func WithWorker(w Worker) CommandCollectorConfiguration {
	return func(os *CommandCollector) error {
		os.worker = w
		return nil
	}
}

func WithCollectors(c []Collector) CommandCollectorConfiguration {
	return func(os *CommandCollector) error {
		os.collectors = c
		return nil
	}
}
