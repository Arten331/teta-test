package kafka

import (
	"context"
	"errors"
	"fmt"
	"github.com/segmentio/kafka-go"
	"go.uber.org/zap"

	"shop/pkg/logger"
)

type QueueableMessage interface {
	KafkaMessage() (kafka.Message, error)
}

type Producer struct {
	Writer           *kafka.Writer
	BeforeSendAction func(context.Context, []kafka.Message)
}

type ProducerClientOptions struct {
	Brokers    []string
	Topic      string
	BeforeSend func(context.Context, []kafka.Message)
}

func NewProducer(o ProducerClientOptions) (Producer, error) {
	if len(o.Brokers) == 0 || o.Brokers[0] == "" || o.Topic == "" {
		return Producer{}, errors.New("kafka connection parameters not specified")
	}

	c := Producer{
		BeforeSendAction: o.BeforeSend,
	}

	logInfo := kafka.LoggerFunc(func(s string, i ...interface{}) {
		logger.L().Debug(fmt.Sprintf(s, i...))
	})

	logError := kafka.LoggerFunc(func(s string, i ...interface{}) {
		logger.L().Error(fmt.Sprintf(s, i...))
	})

	c.Writer = &kafka.Writer{
		ErrorLogger:  logError,
		Logger:       logInfo,
		MaxAttempts:  10,
		RequiredAcks: kafka.RequireAll,
		Addr:         kafka.TCP(o.Brokers[0]),
		Topic:        o.Topic,
		Balancer:     &kafka.LeastBytes{},
	}

	return c, nil
}

func MustCreateProducer(o ProducerClientOptions) Producer {
	p, err := NewProducer(o)
	if err != nil {
		panic(err)
	}

	return p
}

func (c *Producer) SendMessages(ctx context.Context, msgs []QueueableMessage) error {
	queueMessages, err := MessagesFromMany(msgs)
	if err != nil {
		logger.L().Error(
			"queue producer unable create messages",
			zap.Array("messages", zapQueueMessages(queueMessages)),
			zap.String("topic", c.Writer.Topic),
			zap.Error(err),
		)

		return err
	}

	c.BeforeSendAction(ctx, queueMessages)

	err = c.Writer.WriteMessages(ctx, queueMessages...)
	if err != nil {
		logger.L().Error(
			"Unable write messages",
			zap.Array("messages", zapQueueMessages(queueMessages)),
			zap.String("topic", c.Writer.Topic),
			zap.Error(err),
		)

		return err
	}

	logger.L().Debug("Write queue message",
		zap.Array("messages", zapQueueMessages(queueMessages)),
		zap.String("topic", c.Writer.Topic),
	)

	return nil
}

func MessagesFromMany(messages []QueueableMessage) ([]kafka.Message, error) {
	result := make([]kafka.Message, 0, len(messages))

	for _, message := range messages {
		kafkaMessage, err := message.KafkaMessage()
		if err != nil {
			return nil, err
		}

		result = append(result, kafkaMessage)
	}

	return result, nil
}
