package workpool

import (
	"context"
	"crypto/rand"
	"encoding/binary"
	"fmt"
	"runtime"

	"shop/pkg/logger"
)

type ProcessableCommand interface {
	Process() error
}

type WorkerPool struct {
	maxWorkers int
	taskQueue  []chan ProcessableCommand
	cancel     context.CancelFunc
	ctx        context.Context
}

type Options struct {
	MaxWorkers int
	CancelFunc context.CancelFunc
}

func NewWorkerPool(o Options) *WorkerPool {
	numCPU := runtime.NumCPU()

	maxWorkers := o.MaxWorkers

	if maxWorkers < 1 {
		logger.L().Info(
			fmt.Sprintf("Max workers equals 0, set workers to NumCPU: %d", numCPU),
		)

		maxWorkers = numCPU
	} else if maxWorkers > runtime.NumCPU() {
		logger.L().Info(
			fmt.Sprintf("Max workers more than the number of cores, set workers to NumCPU: %d", numCPU),
		)

		maxWorkers = numCPU
	}

	pool := &WorkerPool{
		taskQueue:  make([]chan ProcessableCommand, maxWorkers),
		maxWorkers: maxWorkers,
		cancel:     o.CancelFunc,
	}

	return pool
}

func (p WorkerPool) Stop() {
	p.cancel()
}

func (p *WorkerPool) Submit(task ProcessableCommand) {
	idx := randUInt64() % uint64(p.maxWorkers)

	if task != nil {
		p.taskQueue[idx] <- task
	}
}

func (p WorkerPool) Run(ctx context.Context) *WorkerPool {
	p.ctx = ctx
	for i := 0; i < p.maxWorkers; i++ {
		p.taskQueue[i] = make(chan ProcessableCommand)
		go p.startWorker(p.taskQueue[i])
	}

	return &p
}

func (p WorkerPool) startWorker(taskChan chan ProcessableCommand) {
	go func() {
		for {
			select {
			case task := <-taskChan:
				err := task.Process()
				if err != nil {
					continue
				}
			case <-p.ctx.Done():
				return
			}
		}
	}()
}

func randUInt64() uint64 {
	buf := make([]byte, 8)

	_, _ = rand.Read(buf)

	return binary.LittleEndian.Uint64(buf)
}
