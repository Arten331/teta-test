package logger

import (
	"fmt"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"log"
	"time"
)

const (
	KeyLevelError = "ERROR"
	KeyLevelInfo  = "INFO"
	KeyLevelDebug = "DEBUG"
)

// l default logger
var l *zap.Logger

/*FormatterErrors*/
var (
	ErrWrongLogLevelConfiguration = func(opts ...interface{}) error {
		return fmt.Errorf("wrong logger level: %s, instead [%s]", opts...)
	}
)

type Options struct {
	Level string
	Debug bool
}

func SetupDefaultLogger(o Options) error {
	var logger *zap.Logger
	var encoderConfig zapcore.EncoderConfig

	encoderConfig = zap.NewProductionEncoderConfig()
	encoderConfig.EncodeLevel = zapcore.LowercaseColorLevelEncoder
	encoderConfig.EncodeDuration = zapcore.StringDurationEncoder
	encoderConfig.EncodeTime = zapcore.TimeEncoderOfLayout(time.StampMilli)

	dLevel, err := zapcore.ParseLevel(o.Level)
	if err != nil {
		log.Println(
			ErrWrongLogLevelConfiguration(
				o.Level,
				[]string{KeyLevelError, KeyLevelInfo, KeyLevelDebug},
			),
		)
		return err
	}

	config := zap.Config{
		Level:       zap.NewAtomicLevelAt(dLevel),
		Development: o.Debug,
		Sampling: &zap.SamplingConfig{
			Initial:    100,
			Thereafter: 100,
		},
		Encoding:         "console",
		EncoderConfig:    encoderConfig,
		OutputPaths:      []string{"stderr"},
		ErrorOutputPaths: []string{"stderr"},
	}

	logger, err = config.Build()

	l = logger

	return err
}

func L() *zap.Logger {
	return l
}

func S() *zap.SugaredLogger {
	return l.Sugar()
}
