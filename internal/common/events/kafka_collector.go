package events

import (
	"context"
	"github.com/segmentio/kafka-go"
	"go.uber.org/zap"

	"shop/internal/common/tracer"
	"shop/pkg/collector"
	kafkaClient "shop/pkg/kafka"
	"shop/pkg/logger"
)

type KafkaEventCollectorConfiguration func(c *KafkaEventCollector) error

type KafkaEventCollector struct {
	publisher     *EventPublisher
	consumer      kafkaClient.Consumer
	checkedEvents func(message kafka.Message) QueueableEvent
}

type EventCommand struct {
	context   context.Context
	publisher *EventPublisher
	event     Event
	onProcess func(ctx context.Context) // eg. for commit kafka message
}

func (e EventCommand) Process() error {
	e.publisher.Notify(e.context, e.event)
	e.onProcess(e.context)

	return nil
}

func NewOrderKafkaEventCollector(
	c kafkaClient.Consumer,
	p *EventPublisher,
	cfgs ...KafkaEventCollectorConfiguration,
) (KafkaEventCollector, error) {
	var err error

	ec := KafkaEventCollector{
		publisher: p,
		consumer:  c,
	}

	// Apply all Configurations passed in
	for _, cfg := range cfgs {
		err = cfg(&ec)
		if err != nil {
			return KafkaEventCollector{}, err
		}
	}

	return ec, nil
}

func (c KafkaEventCollector) EventCommandFromMessage(ctx context.Context, message kafka.Message) (*EventCommand, error) {
	var err error
	var event Event

	queueEvent := c.checkedEvents(message)

	if queueEvent == nil {
		c.consumer.CommitMessage(ctx, message)
		return nil, err
	}

	event, err = queueEvent.FromMessage(message)
	if err != nil {
		return nil, err
	}

	ctx, _ = tracer.GetSpanFromKafkaMessage(ctx, message)

	eventCommand := EventCommand{
		context:   ctx,
		publisher: c.publisher,
		event:     event,
		onProcess: func(ctx context.Context) {
			c.consumer.CommitMessage(ctx, message)
		},
	}

	return &eventCommand, nil
}

func (c *KafkaEventCollector) Collect(ctx context.Context, ch chan collector.ProcessableCommand) {
	go func() {
		for {

			select {
			case <-ctx.Done():
				return
			default:
				msg, err := c.consumer.FetchMessage(ctx)
				if err != nil {
					logger.L().Error("Error fetch message",
						zap.String("topic", c.consumer.Reader.Config().Topic),
						zap.Error(err),
					)

					continue
				}

				eventCommand, err := c.EventCommandFromMessage(ctx, msg)
				if err != nil {
					logger.L().Error("Unable parse event message, commit anyway",
						zap.String("topic", msg.Topic),
						zap.ByteString("key", msg.Key),
						zap.ByteString("message", msg.Value),
						zap.Error(err),
					)

					err := c.consumer.Reader.CommitMessages(ctx, msg)
					if err != nil {
						logger.L().Error("Unable commit message", zap.Error(err))

						continue
					}
				}

				if eventCommand == nil { // Unprocessable event - skip
					continue
				}

				ch <- eventCommand
			}
		}
	}()
}

func WithCheckedEvents(checkedEvents func(message kafka.Message) QueueableEvent) KafkaEventCollectorConfiguration {
	return func(c *KafkaEventCollector) error {
		c.checkedEvents = checkedEvents
		return nil
	}
}
