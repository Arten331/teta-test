package order

import (
	"encoding/json"
	"github.com/segmentio/kafka-go"

	"shop/internal/common/events"
)

const (
	KeyOrderCreated                = "event.order.created"
	KeyOrderItemsReservedInStorage = "event.order.items-reserved-in-storage"
	KeyOrderItemsReserveRejected   = "event.order.items-reserve-in-storage-rejected"
	KeyOrderPaymentSuccess         = "event.order.payment-success"
	KeyOrderPaymentRejected        = "event.order.payment-rejected"
)

type Event interface {
	OrderID() uint64
	events.Event
	events.QueueableEvent
}

// Как я понял событие должно быть immutable
// НО это влечет создание отдельных структур данных + куча сетеров/гетеров + более сложная логика парсинга сообщений
// Я на данном этапе себе такое не могу. Поэтому orderID стал ID 🤷
// Дисклеймер: Количество событий максимально минимизировано и не соответствует жестокой реальности)

type Created struct {
	ID         uint64             `json:"order_id"`
	OrderItems []ReserveOrderItem `json:"order_items,omitempty"`
}

type ReserveOrderItem struct {
	ItemID   uint64 `json:"item_id,omitempty"`
	Quantity int    `json:"quantity,omitempty"`
}

func (e Created) Name() string {
	return KeyOrderCreated
}

func (e Created) OrderID() uint64 {
	return e.ID
}

func (e Created) FromMessage(message kafka.Message) (events.Event, error) {
	err := json.Unmarshal(message.Value, &e)
	return e, err
}

func (e Created) KafkaMessage() (kafka.Message, error) {
	jsonMessageView, err := json.Marshal(e)
	if err != nil {
		return kafka.Message{}, err
	}

	return kafka.Message{
		Key:   []byte(e.Name()),
		Value: jsonMessageView,
	}, nil
}

type ReservedInStorage struct {
	ID         uint64 `json:"order_id"`
	CustomerID uint64 `json:"customer_id"`
	Amount     int    `json:"amount"`
}

func (e ReservedInStorage) Name() string {
	return KeyOrderItemsReservedInStorage
}

func (e ReservedInStorage) OrderID() uint64 {
	return e.ID
}

func (e ReservedInStorage) FromMessage(message kafka.Message) (events.Event, error) {
	err := json.Unmarshal(message.Value, &e)
	return e, err
}

func (e ReservedInStorage) KafkaMessage() (kafka.Message, error) {
	jsonMessageView, err := json.Marshal(e)
	if err != nil {
		return kafka.Message{}, err
	}

	return kafka.Message{
		Key:   []byte(e.Name()),
		Value: jsonMessageView,
	}, nil
}

type ReserveInStorageRejected struct {
	ID uint64 `json:"order_id"`
}

func (e ReserveInStorageRejected) Name() string {
	return KeyOrderItemsReserveRejected
}

func (e ReserveInStorageRejected) OrderID() uint64 {
	return e.ID
}

func (e ReserveInStorageRejected) FromMessage(message kafka.Message) (events.Event, error) {
	err := json.Unmarshal(message.Value, &e)
	return e, err
}

func (e ReserveInStorageRejected) KafkaMessage() (kafka.Message, error) {
	jsonMessageView, err := json.Marshal(e)
	if err != nil {
		return kafka.Message{}, err
	}

	return kafka.Message{
		Key:   []byte(e.Name()),
		Value: jsonMessageView,
	}, nil
}

type PaymentSuccess struct {
	ID uint64 `json:"order_id"`
}

func (e PaymentSuccess) Name() string {
	return KeyOrderPaymentSuccess
}

func (e PaymentSuccess) OrderID() uint64 {
	return e.ID
}

func (e PaymentSuccess) FromMessage(message kafka.Message) (events.Event, error) {
	err := json.Unmarshal(message.Value, &e)
	return e, err
}

func (e PaymentSuccess) KafkaMessage() (kafka.Message, error) {
	jsonMessageView, err := json.Marshal(e)
	if err != nil {
		return kafka.Message{}, err
	}

	return kafka.Message{
		Key:   []byte(e.Name()),
		Value: jsonMessageView,
	}, nil
}

type PaymentRejected struct {
	ID uint64 `json:"order_id"`
}

func (e PaymentRejected) Name() string {
	return KeyOrderPaymentRejected
}

func (e PaymentRejected) OrderID() uint64 {
	return e.ID
}

func (e PaymentRejected) FromMessage(message kafka.Message) (events.Event, error) {
	err := json.Unmarshal(message.Value, &e)
	return e, err
}

func (e PaymentRejected) KafkaMessage() (kafka.Message, error) {
	jsonMessageView, err := json.Marshal(e)
	if err != nil {
		return kafka.Message{}, err
	}

	return kafka.Message{
		Key:   []byte(e.Name()),
		Value: jsonMessageView,
	}, nil
}
