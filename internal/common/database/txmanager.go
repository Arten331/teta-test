package database

import (
	"context"
	"fmt"
	"go.uber.org/zap"

	"shop/internal/common/tracer"
	"shop/pkg/logger"
)

type TXManager struct {
	connMap      map[TransactionerDB]TransactionerDB // repositoryDB - created TX map
	aConnections []TransactionerDB                   // Active connections
}

func NewTXManager() TXManager {
	return TXManager{}
}

// RunInTransaction Экспериментальный функционал
func (m TXManager) RunInTransaction(ctx context.Context, runningInTX func(ctx context.Context) error, repositories ...TXRepository) error {
	var err error

	m.connMap = make(map[TransactionerDB]TransactionerDB)

	ctx, span := tracer.NewSpan(ctx, fmt.Sprintf("%s.transaction", tracer.GetServiceName()))
	defer span.End()

	for _, r := range repositories {
		err := r.EnableTX(ctx, func(db TransactionerDB) TransactionerDB {
			_, ok := m.connMap[db]
			if ok {
				return db
			}

			nConn, err := db.StartTXSession(ctx)

			if err != nil {
				logger.L().Error(
					"Unable enable TX - repository",
					zap.String("db", fmt.Sprintf("%#v", db)),
					zap.Error(err),
				)

				return db // The connection will work without tx
			}

			m.connMap[db] = nConn
			m.aConnections = append(m.aConnections, nConn)

			return nConn
		})

		if err != nil {
			return err
		}
	}
	defer m.endTransactions(ctx)

	err = m.begin(ctx)
	if err != nil {
		return err
	}

	err = runningInTX(ctx)

	if err == nil {
		return m.commit(ctx)
	}

	m.rollback(ctx)

	return err
}

func (m *TXManager) begin(ctx context.Context) error {
	for _, connection := range m.aConnections {
		err := connection.Begin(ctx)
		if err != nil {
			tracer.SpanFromContext(ctx).RecordError(err)
			return err
		}
	}

	tracer.SpanFromContext(ctx).AddEvent("begin transaction")

	return nil
}

func (m *TXManager) commit(ctx context.Context) error {
	for _, connection := range m.aConnections {
		err := connection.Commit(ctx)
		if err != nil {
			tracer.SpanFromContext(ctx).RecordError(err)
			return err
		}
	}

	tracer.SpanFromContext(ctx).AddEvent("commit transaction")

	return nil
}

func (m *TXManager) rollback(ctx context.Context) {
	for _, connection := range m.aConnections {
		err := connection.Rollback(ctx)
		if err != nil {
			tracer.SpanFromContext(ctx).RecordError(err)

			logger.L().Error(
				"Failed to rollback transaction",
				zap.String("connection", fmt.Sprintf("%#v", connection)),
				zap.Error(err),
			)
		}
	}

	tracer.SpanFromContext(ctx).AddEvent("rollback transaction")
}

func (m *TXManager) endTransactions(ctx context.Context) {
	for _, transactionerDB := range m.connMap {
		transactionerDB.EndTXSession(ctx)
	}
}
