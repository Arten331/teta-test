package mongo

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.uber.org/zap"

	"shop/internal/common/database"
	"shop/pkg/logger"
)

const (
	prefix = "mongodb://"
)

type DB struct {
	clientOptions *options.ClientOptions
	conn          struct {
		dbName  string
		client  *mongo.Client
		session mongo.SessionContext
	}
}

type Options struct {
	DBName   string
	Host     string
	Port     int
	User     string
	Password string
}

func NewMongoDB(ctx context.Context, o Options) (DB, error) {
	db := DB{}

	connectionString := fmt.Sprintf("%[1]s%[2]s:%[3]d/%[4]s", prefix, o.Host, o.Port, o.DBName)
	cOpts := options.Client().ApplyURI(connectionString)

	err := cOpts.Validate()
	if err != nil {
		logger.L().Error(
			"Invalid Database options",
			zap.String("connection", connectionString),
			zap.Error(err),
		)

		return db, err
	}

	cOpts.Auth = &options.Credential{
		AuthMechanism: "SCRAM-SHA-256",
		AuthSource:    o.DBName,
		Username:      o.User,
		Password:      o.Password,
	}

	db.clientOptions = cOpts

	mClient, err := mongo.Connect(ctx, db.clientOptions)
	if err != nil {
		logger.L().Error(
			"Enable connect to mongoDB Database",
			zap.String("connection", connectionString),
			zap.Error(err),
		)

		return db, err
	}

	db.conn.dbName = o.DBName
	db.conn.client = mClient

	return db, nil
}

func (db *DB) InsertOne(ctx context.Context, coll *mongo.Collection, d bson.D) (*mongo.InsertOneResult, error) {
	var result *mongo.InsertOneResult
	var errInsert error

	err := db.runInSession(ctx, func(sc mongo.SessionContext) {
		result, errInsert = coll.InsertOne(sc, d)
	})

	if errInsert != nil {
		return nil, errInsert
	}

	return result, err
}

func (db *DB) InsertMany(ctx context.Context, coll *mongo.Collection, d []interface{}) (*mongo.InsertManyResult, error) {
	var result *mongo.InsertManyResult
	var errInsert error

	err := db.runInSession(ctx, func(sc mongo.SessionContext) {
		result, errInsert = coll.InsertMany(sc, d)
	})

	if errInsert != nil {
		return nil, errInsert
	}

	return result, err
}

func (db *DB) Find(ctx context.Context, coll *mongo.Collection, filter bson.D) (*mongo.Cursor, error) {
	var cursor *mongo.Cursor
	var errFind error

	err := db.runInSession(ctx, func(sc mongo.SessionContext) {
		cursor, errFind = coll.Find(sc, filter)
	})

	if errFind != nil {
		return nil, errFind
	}

	return cursor, err
}

func (db *DB) FindOne(ctx context.Context, coll *mongo.Collection, filter bson.D) (*mongo.SingleResult, error) {
	var result *mongo.SingleResult

	err := db.runInSession(ctx, func(sc mongo.SessionContext) {
		result = coll.FindOne(sc, filter)
	})

	return result, err
}

func (db *DB) UpdateOne(ctx context.Context, coll *mongo.Collection, filter bson.D, item interface{}) (*mongo.UpdateResult, error) {
	var result *mongo.UpdateResult
	var updateErr error

	err := db.runInSession(ctx, func(sc mongo.SessionContext) {
		result, updateErr = coll.UpdateOne(sc, filter, item)
	})

	if updateErr != nil {
		return result, updateErr
	}

	return result, err
}

func (db *DB) Collection(name string, opts ...*options.CollectionOptions) (*mongo.Collection, error) {
	return db.conn.client.Database(db.conn.dbName).Collection(name, opts...), nil
}

func (db *DB) Begin(_ context.Context) error {
	err := db.conn.session.StartTransaction()
	if err != nil {
		return err
	}

	return nil
}

func (db *DB) Rollback(ctx context.Context) error {
	err := db.conn.session.AbortTransaction(ctx)
	if err != nil {
		return err
	}

	return nil
}

func (db *DB) Commit(ctx context.Context) error {
	err := db.conn.session.CommitTransaction(ctx)
	if err != nil {
		return err
	}

	return nil
}

func (db DB) StartTXSession(ctx context.Context) (database.TransactionerDB, error) {
	session, err := db.conn.client.StartSession()

	if err != nil {
		return nil, err
	}

	db.conn.session = mongo.NewSessionContext(ctx, session)

	return &db, nil
}

func (db *DB) EndTXSession(ctx context.Context) {
	db.conn.session.EndSession(ctx)
	db.conn.session = nil
}

func (db *DB) runInSession(ctx context.Context, running func(sc mongo.SessionContext)) error {
	var err error

	if db.conn.session == nil {
		err = db.conn.client.UseSession(ctx, func(sessionContext mongo.SessionContext) error {
			running(sessionContext)
			return nil
		})
	}
	if err != nil {
		return err
	}

	running(db.conn.session)

	return nil
}
