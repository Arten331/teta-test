package mongoRepo

import (
	"context"
	"errors"

	"shop/internal/common/database"
)

type Repository struct {
	db database.MongoDriverDB
}

func (r *Repository) EnableTX(
	_ context.Context,
	fnEnable func(db database.TransactionerDB) database.TransactionerDB,
) error {
	nConn := fnEnable(r.db)
	if nConn != nil {
		nConn, ok := nConn.(database.MongoDriverDB)
		if !ok {
			return errors.New("Unable enable TX in repository, wrong db type")
		}
		r.db = nConn
	}
	return nil
}

func WithDatabase(db database.MongoDriverDB) RepositoryConfiguration {
	return func(r *Repository) error {
		r.db = db
		return nil
	}
}

type RepositoryConfiguration func(cr *Repository) error

func (r *Repository) Conn() database.MongoConn {
	return r.db
}
