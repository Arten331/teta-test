package pgx

import (
	"context"
	"errors"

	"shop/internal/common/database"
)

type Repository struct {
	db database.PgxDB
}

func (p *Repository) EnableTX(
	_ context.Context,
	fnEnable func(db database.TransactionerDB) database.TransactionerDB,
) error {
	nConn := fnEnable(p.db)
	if nConn != nil {
		nConn, ok := nConn.(database.PgxDB)
		if !ok {
			return errors.New("Unable enable TX in repository, wrong db type")
		}
		p.db = nConn
	}

	return nil
}

func WithDatabase(db database.PgxDB) RepositoryConfiguration {
	return func(r *Repository) error {
		r.db = db
		return nil
	}
}

type RepositoryConfiguration func(cr *Repository) error

func (p *Repository) Conn() database.PgxDBConn {
	return p.db
}
