package database

import (
	"context"
	"github.com/jackc/pgconn"
	"github.com/jackc/pgx/v4"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// TXRepository (TransactionerRepository) supply transactions with TXManager
// Repository need ability replace own connection/db
type TXRepository interface {
	EnableTX(ctx context.Context, fnEnable func(db TransactionerDB) TransactionerDB) error
}

type TransactionerDB interface {
	Transactioner
	StartTXSession(ctx context.Context) (TransactionerDB, error) // CreateTX create new copy DB connection marked as TXer
	EndTXSession(ctx context.Context)
}

type Transactioner interface {
	Begin(ctx context.Context) error
	Rollback(ctx context.Context) error
	Commit(ctx context.Context) error
}

// PgxDB pgx-clients database connections
type PgxDB interface {
	PgxDBConn
	TransactionerDB
}

// MongoDriverDB PgxDB mongo-clients database connections
type MongoDriverDB interface {
	MongoConn
	TransactionerDB
}

type PgxDBConn interface {
	Exec(ctx context.Context, sql string, args ...interface{}) (pgconn.CommandTag, error)
	Query(ctx context.Context, sql string, args ...interface{}) (pgx.Rows, error)
	QueryRow(ctx context.Context, sql string, args ...interface{}) pgx.Row
}

type MongoConn interface {
	InsertOne(ctx context.Context, coll *mongo.Collection, d bson.D) (*mongo.InsertOneResult, error)
	InsertMany(ctx context.Context, coll *mongo.Collection, d []interface{}) (*mongo.InsertManyResult, error)
	Find(ctx context.Context, coll *mongo.Collection, filter bson.D) (*mongo.Cursor, error)
	FindOne(ctx context.Context, coll *mongo.Collection, filter bson.D) (*mongo.SingleResult, error)
	UpdateOne(ctx context.Context, coll *mongo.Collection, filter bson.D, item interface{}) (*mongo.UpdateResult, error)
	Collection(name string, opts ...*options.CollectionOptions) (*mongo.Collection, error)
}
