package postgres

import (
	"context"
	"fmt"
	"github.com/jackc/pgconn"
	"github.com/jackc/pgtype/pgxtype"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"go.opentelemetry.io/otel/attribute"
	"go.uber.org/zap"

	"shop/internal/common/database"
	"shop/internal/common/tracer"
	"shop/pkg/logger"
)

type DB struct {
	dbName string
	pool   *pgxpool.Pool
	dbConn
}

type dbConn struct {
	txStarted bool // flag for TX lazy start
	tx        pgx.Tx
	conn      *pgxpool.Conn
}

type DBOptions struct {
	DBName          string
	Host            string
	Port            int
	User            string
	Password        string
	ConnTimeout     int
	PoolMaxConn     int
	PoolMinConn     int
	PoolLazyConnect bool
}

const (
	DBConnPrefix = "postgresql"
)

func (db *DB) Begin(ctx context.Context) error {
	if db.tx != nil && !db.tx.Conn().IsClosed() {
		return nil
	}

	tx, err := db.pool.BeginTx(ctx, pgx.TxOptions{
		IsoLevel: pgx.ReadCommitted,
	})
	if err != nil {
		return err
	}

	db.tx = tx

	return nil
}

func (db *DB) Rollback(ctx context.Context) error {
	err := db.tx.Rollback(ctx)
	db.tx = nil
	if err != nil {
		return err
	}
	return nil
}

func (db *DB) Commit(ctx context.Context) error {
	err := db.tx.Commit(ctx)
	db.tx = nil
	if err != nil {
		return err
	}
	return nil
}

func (db *DB) Exec(ctx context.Context, sql string, args ...interface{}) (pgconn.CommandTag, error) {
	var result pgconn.CommandTag

	_, span := tracer.NewSpan(ctx, fmt.Sprintf("%s.postgres.query", tracer.GetServiceName()))
	span.SetAttributes(attribute.Key("query").String(sql), attribute.Key("args").String(fmt.Sprintf("%v", args)))
	defer span.End()

	err := db.runQuery(ctx, func(conn pgxtype.Querier) error {
		var err error

		result, err = conn.Exec(ctx, sql, args...)
		if err != nil {
			span.RecordError(err)
			logger.L().Error(
				"Error while execute the query",
				zap.String("query", sql),
				zap.Error(err),
			)
		}

		return err
	})

	return result, err
}

func (db *DB) Query(ctx context.Context, sql string, args ...interface{}) (pgx.Rows, error) {
	var rows pgx.Rows

	_, span := tracer.NewSpan(ctx, fmt.Sprintf("%s.postgres.query", tracer.GetServiceName()))
	span.SetAttributes(attribute.Key("query").String(sql), attribute.Key("args").String(fmt.Sprintf("%v", args)))
	defer span.End()

	err := db.runQuery(ctx, func(conn pgxtype.Querier) error {
		var err error

		rows, err = conn.Query(ctx, sql, args...)
		if err != nil {
			span.RecordError(err)
			logger.L().Error(
				"Error while execute the query",
				zap.String("query", sql),
				zap.Error(err),
			)

			return err
		}

		return nil
	})

	return rows, err
}

func (db *DB) QueryRow(ctx context.Context, sql string, args ...interface{}) pgx.Row {
	var row pgx.Row

	_, span := tracer.NewSpan(ctx, fmt.Sprintf("%s.postgres.query", tracer.GetServiceName()))
	span.SetAttributes(attribute.Key("query").String(sql), attribute.Key("args").String(fmt.Sprintf("%v", args...)))
	defer span.End()

	err := db.runQuery(ctx, func(conn pgxtype.Querier) error {
		row = conn.QueryRow(ctx, sql, args...)

		return nil
	})
	if err != nil {
		span.RecordError(err)
	}

	return row
}

func NewPostgresDB(ctx context.Context, o DBOptions) (DB, error) {
	db := DB{
		dbName: o.DBName,
	}

	// Check options
	if o.ConnTimeout == 0 {
		o.ConnTimeout = 30
	}

	if o.PoolMaxConn == 0 {
		o.PoolMaxConn = 3
	}

	connString := fmt.Sprintf(
		"%[1]s://%[2]s@%[3]s:%[4]d/%[5]s?connect_timeout=%[6]d",
		DBConnPrefix,
		o.User,
		o.Host,
		o.Port,
		o.DBName,
		o.ConnTimeout,
	)

	poolCfg, err := pgxpool.ParseConfig(connString)
	if err != nil {
		logger.L().Error(
			"Unable parse pgxPool config",
			zap.String("connection", connString),
			zap.Error(err),
		)

		return DB{}, err
	}

	// Set password later for clear log
	poolCfg.ConnConfig.Password = o.Password

	// Enable logger
	poolCfg.ConnConfig.LogLevel = pgx.LogLevelDebug
	poolCfg.ConnConfig.Logger = db

	poolCfg.MaxConns = int32(o.PoolMaxConn)
	poolCfg.MinConns = int32(o.PoolMinConn)
	poolCfg.LazyConnect = true

	logger.L().Info(
		"Init connection for PostgreSQL",
		zap.String("connection", connString),
	)

	pool, err := pgxpool.ConnectConfig(ctx, poolCfg)
	if err != nil {
		logger.L().Error(
			"Error while creating PostgreSQL pool connection",
			zap.String("poolCfg", fmt.Sprintf("%#v", poolCfg)),
			zap.Error(err),
		)

		return DB{}, err
	}

	db.pool = pool

	return db, nil
}

func (db DB) StartTXSession(ctx context.Context) (database.TransactionerDB, error) {
	err := db.Begin(ctx)
	return &db, err
}

func (db DB) EndTXSession(ctx context.Context) {
	if db.tx != nil {
		db.tx.Conn().Close(ctx)
	}
}

// Репозиторий ничего не знает о транзакция и соединениях
// Поэтому мы должны указать через что (pool/conn/tx?) нужно делать запрос

func (db *DB) runQuery(ctx context.Context, query func(conn pgxtype.Querier) error) error {
	var conn *pgxpool.Conn
	var err error

	// Cлучай открытой транзакции
	if db.tx != nil {
		err = query(db.tx.Conn())
		if err != nil {
			return err
		}
		return nil
	}

	// Случай когда транзакцию не стартовали (auto-commit)
	conn, err = db.pool.Acquire(ctx)
	if err != nil {
		return err
	}
	defer conn.Release()

	err = query(conn)

	return err
}

func (db DB) Log(_ context.Context, _ pgx.LogLevel, msg string, data map[string]interface{}) {
	logger.L().Debug(msg, zap.String("DB", db.dbName), zap.Any("data", data))
}
