package tools

import (
	"fmt"
	"runtime"
)

func GetCalledFuncAndLine() string {
	pc, _, line, _ := runtime.Caller(2)
	details := runtime.FuncForPC(pc)

	return fmt.Sprintf("%s\n:%d", details.Name(), line)
}
