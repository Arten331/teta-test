package tools

import (
	"os"
	"strconv"
)

func GetEnvAsStr(key, defaultVal string) string {
	if value, exists := os.LookupEnv(key); exists {
		return value
	}

	return defaultVal
}

func GetEnvAsInt(key string, defaultVal int) int {
	valueStr := GetEnvAsStr(key, "")
	if value, err := strconv.Atoi(valueStr); err == nil {
		return value
	}

	return defaultVal
}

func GetEnvAsBool(key string, defaultVal bool) bool {
	valStr := GetEnvAsStr(key, "")
	if val, err := strconv.ParseBool(valStr); err == nil {
		return val
	}

	return defaultVal
}
