package tracer

import (
	"context"
	"github.com/segmentio/kafka-go"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

var tracer trace.Tracer
var serviceName string

func SetupGlobalTracer(t trace.Tracer, sn string) {
	serviceName = sn
	tracer = t
}

func GetTracer() trace.Tracer {
	return tracer
}

func GetServiceName() string {
	return serviceName
}

func NewSpan(ctx context.Context, name string) (context.Context, trace.Span) {
	tracer := GetTracer()
	return tracer.Start(ctx, name)
}

func NewSpanWithAttributes(ctx context.Context, name string, attrs ...attribute.KeyValue) (context.Context, trace.Span) {
	var span trace.Span

	tracer := GetTracer()
	ctx, span = tracer.Start(ctx, name)
	span.SetAttributes(attrs...)

	return ctx, span
}

func SpanFromContext(ctx context.Context) trace.Span {
	return trace.SpanFromContext(ctx)
}

func GetSpanFromKafkaMessage(ctx context.Context, msg kafka.Message) (context.Context, trace.Span) {
	// get span context from kafka message headers
	var spanID trace.SpanID
	var traceID trace.TraceID
	var counter int
	var err error

	for i := range msg.Headers {
		switch msg.Headers[i].Key {
		case "span-id":
			spanID, err = trace.SpanIDFromHex(string(msg.Headers[i].Value))
			counter++
			if err != nil {
				break
			}
		case "trace-id":
			traceID, err = trace.TraceIDFromHex(string(msg.Headers[i].Value))
			counter++
			if err != nil {
				break
			}
		}
	}

	if counter == 2 {
		spanContext := trace.NewSpanContext(trace.SpanContextConfig{
			TraceID: traceID,
			SpanID:  spanID,
		})
		ctx = trace.ContextWithSpanContext(ctx, spanContext)
	}

	return ctx, SpanFromContext(ctx)
}

func InjectSpanToKafkaMessages(ctx context.Context, queueMessages []kafka.Message) {
	if SpanFromContext(ctx).IsRecording() {
		spanHeader := SpanFromContext(ctx).SpanContext()
		for i := range queueMessages {
			queueMessages[i].Headers = []kafka.Header{
				{
					Key:   "span-id",
					Value: []byte(spanHeader.SpanID().String()),
				},
				{
					Key:   "trace-id",
					Value: []byte(spanHeader.TraceID().String()),
				},
			}
		}
	}
}
