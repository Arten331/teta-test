package postgres

import (
	"errors"
	"go.uber.org/zap"

	"github.com/jackc/pgx/v4"

	"shop/internal/wallet/domain/wallet"
	"shop/internal/wallet/models"
	"shop/pkg/logger"
)

var ErrFetchAccount = errors.New("unable fetch account")

func (r WalletRepository) fetchAccountRow(row pgx.Rows) (wallet.Wallet, error) {
	var account models.AccountBalance

	err := row.Scan(
		&account.ID,
		&account.UserID,
		&account.BonusPoints,
	)

	if err != nil {
		logger.L().Error(ErrFetchAccount.Error(), zap.Error(err))

		return wallet.Wallet{}, ErrFetchAccount
	}

	// load without log
	w, err := wallet.NewWallet(account, nil)
	if err != nil {
		return wallet.Wallet{}, err
	}

	return w, err
}
