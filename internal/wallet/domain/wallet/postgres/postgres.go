package postgres

import (
	"context"
	"fmt"
	"go.uber.org/zap"

	"shop/internal/common/database/repository/pgx"
	"shop/internal/wallet/domain/wallet"
	"shop/internal/wallet/models"
	"shop/pkg/logger"
)

//  TODO: Environment
const (
	accountTable    = "wallet.account_balance"
	accountLogTable = "wallet.account_balance_log"
)

type WalletRepository struct {
	pgx.Repository
	queries Queries
}

type Queries struct {
	getAccount    string
	insertLog     string
	changeBalance string
}

const (
	queryGetAccount    = `select id, user_id, points from %[1]s ib where user_id = $1 FOR UPDATE;`
	queryInsertLog     = `insert into %[1]s (account_id, operation, income) values ($1, $2, $3) returning id`
	queryChangeBalance = `update %[1]s set points = points + $2 where id = $1 FOR UPDATE;`
)

type WalletRepositoryConfiguration func(r *WalletRepository) error

func (r *WalletRepository) GetByUserID(ctx context.Context, userID uint64) (wallet.Wallet, error) {
	var err error
	var w wallet.Wallet

	rows, err := r.Conn().Query(ctx, r.queries.getAccount, userID)
	if err != nil {
		logger.L().Error(wallet.ErrGetAccount.Error(), zap.Uint64("user_id", userID), zap.Error(err))

		return w, wallet.ErrGetAccount
	}

	if !rows.Next() {
		return w, wallet.ErrAccountNotFound
	}

	w, err = r.fetchAccountRow(rows)
	if err != nil {
		logger.L().Error(wallet.ErrGetAccount.Error(), zap.Uint64("user_id", userID), zap.Error(err))

		return w, wallet.ErrGetAccount
	}
	rows.Close()

	return w, nil
}

func (r *WalletRepository) WriteAccountLog(ctx context.Context, log []models.AccountLog) error {
	var err error

	var id uint64

	for i := range log {
		row := r.Conn().QueryRow(ctx, r.queries.insertLog,
			log[i].AccountID,
			log[i].Operation,
			log[i].Income,
		)

		err = row.Scan(&id)
		if err != nil {
			logger.L().Error(wallet.ErrWriteAccountLog.Error(), zap.Any("log", log), zap.Error(err))

			return wallet.ErrWriteAccountLog
		}

		commandTag, err := r.Conn().Exec(context.TODO(), r.queries.changeBalance,
			log[i].AccountID,
			log[i].Income,
		)
		if err != nil || commandTag.RowsAffected() != 1 {
			logger.L().Error(wallet.ErrChangeBalance.Error(), zap.Any("log", log), zap.Error(err))

			return wallet.ErrChangeBalance
		}
	}

	return nil
}

func NewWalletRepository(cfgs ...pgx.RepositoryConfiguration) (WalletRepository, error) {
	r := WalletRepository{
		Repository: pgx.Repository{},
		queries: Queries{
			getAccount:    fmt.Sprintf(queryGetAccount, accountTable),
			insertLog:     fmt.Sprintf(queryInsertLog, accountLogTable),
			changeBalance: fmt.Sprintf(queryChangeBalance, accountTable),
		},
	}

	for _, cfg := range cfgs {
		err := cfg(&r.Repository)
		if err != nil {
			return r, err
		}
	}

	return r, nil
}
