package wallet

import (
	"context"
	"errors"

	"shop/internal/wallet/models"
)

var (
	ErrWriteAccountLog = errors.New("Error write balance log")
	ErrChangeBalance   = errors.New("Error change balance")
	ErrGetAccount      = errors.New("Error get item balance")
	ErrAccountNotFound = errors.New("item balance not found")
)

type Repository interface {
	GetByUserID(ctx context.Context, userID uint64) (Wallet, error)
	WriteAccountLog(ctx context.Context, log []models.AccountLog) error
}
