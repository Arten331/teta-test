package wallet

import (
	"errors"

	"shop/internal/common/events/order"
	"shop/internal/wallet/models"
)

var ErrMissingValues = errors.New("missing values")

type Wallet struct {
	account   models.AccountBalance
	log       []*models.AccountLog
	logLoaded bool
}

func NewWallet(account models.AccountBalance, log []*models.AccountLog) (Wallet, error) {
	if account.ID == 0 || account.UserID == 0 {
		return Wallet{}, ErrMissingValues
	}

	return Wallet{
		account:   account,
		log:       log,
		logLoaded: log != nil,
	}, nil
}

func NewPaymentRejected(orderID uint64) order.PaymentRejected {
	return order.PaymentRejected{
		ID: orderID,
	}
}

func NewPaymentSuccess(orderID uint64) order.PaymentSuccess {
	return order.PaymentSuccess{
		ID: orderID,
	}
}

func (p *Wallet) LoadBalanceLog(log []*models.AccountLog) {
	p.log = log
	p.logLoaded = true
}

func (p Wallet) AccountID() uint64 {
	return p.account.ID
}

func (p Wallet) PointsAvailable() int {
	return p.account.BonusPoints
}

func (p Wallet) Log() []*models.AccountLog {
	return p.log
}

func (p Wallet) LogLoaded() bool {
	return p.logLoaded
}
