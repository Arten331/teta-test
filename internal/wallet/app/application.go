package app

import (
	"context"
	"errors"
	"fmt"
	"github.com/segmentio/kafka-go"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracegrpc"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.7.0"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"shop/internal/common/database/postgres"
	"shop/internal/common/database/repository/pgx"
	"shop/internal/common/events"
	orderEvents "shop/internal/common/events/order"
	"shop/internal/common/tracer"
	"shop/internal/wallet/config"
	"shop/internal/wallet/domain/wallet"
	postgresWallet "shop/internal/wallet/domain/wallet/postgres"
	walletService "shop/internal/wallet/services/wallet"
	"shop/pkg/collector"
	kafkaClient "shop/pkg/kafka"
	"shop/pkg/logger"
	"shop/pkg/workpool"
)

type Databases struct {
	wallet postgres.DB
}

type Repositories struct {
	wallet wallet.Repository
}

type Services struct {
	wallet          *walletService.WalletService
	eventsCollector collector.CommandCollector
}

type Application struct {
	serviceName    string
	cfg            config.AppConfig
	databases      Databases
	repositories   Repositories
	services       Services
	tracerProvider *sdktrace.TracerProvider
	events         struct {
		publisher      events.EventPublisher
		kafkaCollector events.KafkaEventCollector
	}
}

func Init(ctx context.Context, cfg config.AppConfig) (*Application, error) {
	var err error

	ac := &Application{
		serviceName: cfg.ServiceName,
		cfg:         cfg,
	}

	err = ac.initDatabases(ctx, cfg)
	if err != nil {
		return nil, err
	}
	err = ac.initRepositories()
	if err != nil {
		return nil, err
	}

	err = ac.initTracerProvider(ctx)
	if err != nil {
		return nil, err
	}

	ac.initEventsServices(ctx)

	err = ac.initServices(ctx, cfg)
	if err != nil {
		return nil, err
	}

	return ac, nil
}

func (a *Application) initDatabases(ctx context.Context, cfg config.AppConfig) error {
	walletDBConf := cfg.DB.Wallet
	postgresOptions := postgres.DBOptions{
		DBName:          walletDBConf.DBName,
		Host:            walletDBConf.Host,
		Port:            walletDBConf.Port,
		User:            walletDBConf.User,
		Password:        walletDBConf.Password,
		ConnTimeout:     walletDBConf.ConnTimeout,
		PoolMaxConn:     walletDBConf.MaxConnections,
		PoolMinConn:     walletDBConf.MinConnection,
		PoolLazyConnect: true,
	}

	walletDB, err := postgres.NewPostgresDB(ctx, postgresOptions)
	if err != nil {
		logger.L().Error("Error templateRepository DB configuration", zap.Error(err))
	}

	a.databases = Databases{
		wallet: walletDB,
	}

	return nil
}

func (a *Application) initRepositories() error {
	walletRepository, err := postgresWallet.NewWalletRepository(
		pgx.WithDatabase(&a.databases.wallet),
	)

	if err != nil {
		return err
	}

	a.repositories = Repositories{
		wallet: &walletRepository,
	}

	return nil
}

func (a *Application) initEventsServices(_ context.Context) {
	brokers := []string{
		fmt.Sprintf("%s:%d", a.cfg.QueueService.KafkaBroker.Host, a.cfg.QueueService.KafkaBroker.Port),
	}

	cfgQueue := a.cfg.QueueService

	a.events.publisher = events.NewEventPublisher()

	// Обработчик событий kafka (публикует в топик)
	eventSender := events.NewKafkaEventHandler(kafkaClient.MustCreateProducer(
		kafkaClient.ProducerClientOptions{
			Brokers:    brokers,
			Topic:      cfgQueue.Topics.OrderEvents.Name,
			BeforeSend: tracer.InjectSpanToKafkaMessages,
		},
	))

	// Обозначим события которые необходимо выносить в очередь
	a.events.publisher.Subscribe(eventSender,
		orderEvents.PaymentSuccess{},
		orderEvents.PaymentRejected{},
	)

	orderEventConsumer := kafkaClient.MustCreateConsumer(kafkaClient.ConsumerClientOptions{
		Brokers: brokers,
		Topic:   cfgQueue.Topics.OrderEvents.Name,
		Group:   cfgQueue.Topics.OrderEvents.Group,
	})

	orderCheckedEvents := events.WithCheckedEvents(func(message kafka.Message) events.QueueableEvent {
		var event events.QueueableEvent

		//  Список ключей и события которые пытаемся обработать с очереди
		switch string(message.Key) {
		case orderEvents.KeyOrderItemsReservedInStorage:
			event = orderEvents.ReservedInStorage{}
		}

		return event
	})

	a.events.kafkaCollector, _ = events.NewOrderKafkaEventCollector(
		orderEventConsumer,
		&a.events.publisher,
		orderCheckedEvents,
	)
}

func (a *Application) initTracerProvider(ctx context.Context) error {
	res, err := resource.New(ctx,
		resource.WithAttributes(
			semconv.ServiceNameKey.String(a.cfg.ServiceName),
		),
	)
	if err != nil {
		return err
	}

	conn, err := grpc.DialContext(
		ctx,
		a.cfg.OTELTraceCollector,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithBlock(),
	)
	if err != nil {
		return errors.New("failed to create gRPC connection to collector")
	}

	// Set up a trace exporter
	traceExporter, err := otlptracegrpc.New(ctx, otlptracegrpc.WithGRPCConn(conn))
	if err != nil {
		return errors.New("failed to create trace exporter")
	}

	// Register the trace exporter with a TracerProvider, using a batch
	// span processor to aggregate spans before export.
	bsp := sdktrace.NewBatchSpanProcessor(traceExporter)
	tracerProvider := sdktrace.NewTracerProvider(
		sdktrace.WithSampler(sdktrace.AlwaysSample()),
		sdktrace.WithResource(res),
		sdktrace.WithSpanProcessor(bsp),
	)
	otel.SetTracerProvider(tracerProvider)

	a.tracerProvider = tracerProvider

	// set global propagator to tracecontext (the default is no-op).
	otel.SetTextMapPropagator(propagation.NewCompositeTextMapPropagator(propagation.TraceContext{}, propagation.Baggage{}))

	// setup global tracer for service
	tracer.SetupGlobalTracer(otel.Tracer(a.cfg.ServiceName), a.cfg.ServiceName)

	return nil
}

func (a *Application) initServices(_ context.Context, _ config.AppConfig) error {
	wService, err := walletService.NewWalletService(
		walletService.WithWalletRepository(a.repositories.wallet),
		walletService.WithEventPublisher(&a.events.publisher),
	)
	if err != nil {
		return err
	}

	eventCollector, err := collector.NewCommandCollector(
		collector.WithCollectors([]collector.Collector{&a.events.kafkaCollector}),
	)
	if err != nil {
		return err
	}

	a.services = Services{
		wallet:          &wService,
		eventsCollector: eventCollector,
	}

	return err
}

func (a *Application) Run(ctx context.Context, cancelFunc context.CancelFunc) error {
	collectorWorkerPool := workpool.NewWorkerPool(workpool.Options{
		MaxWorkers: 0,
		CancelFunc: cancelFunc,
	})

	err := collector.WithWorker(collectorWorkerPool.Run(ctx))(&a.services.eventsCollector)
	if err != nil {
		return err
	}

	go a.services.eventsCollector.Run(ctx, cancelFunc)

	return nil
}

func (a *Application) Shutdown(_ context.Context) error {
	return nil
}
