package models

type AccountBalance struct {
	ID          uint64
	UserID      uint64
	BonusPoints int
}
