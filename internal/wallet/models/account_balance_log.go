package models

const OperationIncome = "income"
const OperationOutcome = "outcome"

type AccountLog struct {
	ID        uint64
	AccountID uint64
	Operation string
	Income    int
}
