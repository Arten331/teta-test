package wallet

import (
	"context"
	"go.opentelemetry.io/otel/attribute"

	"shop/internal/common/events"
	"shop/internal/common/events/order"
	"shop/internal/common/tracer"
	"shop/internal/wallet/domain/wallet"
)

type EventHandler struct {
	walletService WalletService
	publisher     *events.EventPublisher
}

func NewOrderReservedHandler(w WalletService, p *events.EventPublisher) *EventHandler {
	return &EventHandler{walletService: w, publisher: p}
}

func (eh EventHandler) Notify(ctx context.Context, event events.Event) {
	switch event.(type) {
	case order.ReservedInStorage:
		orderReserved := event.(order.ReservedInStorage)
		orderID := orderReserved.OrderID()

		ctx, span := tracer.NewSpan(ctx, "shop.wallet.payment")
		span.SetAttributes(attribute.Key("orderID").Int64(int64(orderReserved.OrderID())))
		defer span.End()

		err := eh.walletService.PayOrder(ctx, orderReserved.CustomerID, orderReserved.Amount)
		if err != nil {
			eh.publisher.Notify(ctx, wallet.NewPaymentRejected(orderID))
			return
		}

		eh.publisher.Notify(ctx, wallet.NewPaymentSuccess(orderID))
	}
}
