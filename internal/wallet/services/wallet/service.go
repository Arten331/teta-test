package wallet

import (
	"context"
	"errors"
	"go.uber.org/zap"

	"shop/internal/common/events"
	"shop/internal/wallet/domain/wallet"
	"shop/internal/wallet/models"
	"shop/pkg/logger"
)

type WalletService struct {
	wallet         wallet.Repository
	eventPublisher *events.EventPublisher
}

var ErrNotEnoughPointsOnAccount = errors.New("Not enough funds on the account")

func (s *WalletService) PayOrder(ctx context.Context, customerID uint64, amount int) error {
	var err error
	var customerWallet wallet.Wallet
	var accountLog []models.AccountLog

	customerWallet, err = s.wallet.GetByUserID(ctx, customerID)
	if err != nil {
		return err
	}

	if customerWallet.PointsAvailable() < amount {
		return ErrNotEnoughPointsOnAccount
	}

	accountLog = append(accountLog, models.AccountLog{
		AccountID: customerWallet.AccountID(),
		Operation: models.OperationOutcome,
		Income:    -amount,
	})

	err = s.wallet.WriteAccountLog(ctx, accountLog)
	if err != nil {
		logger.L().Error("error write account log", zap.Error(err), zap.Any("log", accountLog))

		return err
	}

	return nil
}
