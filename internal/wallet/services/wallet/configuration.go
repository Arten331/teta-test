package wallet

import (
	"shop/internal/common/events"
	orderEvents "shop/internal/common/events/order"
	"shop/internal/wallet/domain/wallet"
)

// WalletConfiguration is an alias for a function that will take in a pointer to an WalletService and modify it
type WalletConfiguration func(os *WalletService) error

func NewWalletService(cfgs ...WalletConfiguration) (WalletService, error) {
	s := WalletService{}
	// Apply all Configurations passed in
	for _, cfg := range cfgs {
		err := cfg(&s)
		if err != nil {
			return s, err
		}
	}

	// Подпишем наш сервис на события
	s.eventPublisher.Subscribe(NewOrderReservedHandler(s, s.eventPublisher),
		orderEvents.ReservedInStorage{}, // Заказ создан
	)

	return s, nil
}

func WithWalletRepository(w wallet.Repository) WalletConfiguration {
	return func(os *WalletService) error {
		os.wallet = w
		return nil
	}
}

func WithEventPublisher(p *events.EventPublisher) WalletConfiguration {
	return func(s *WalletService) error {
		s.eventPublisher = p
		return nil
	}
}
