package models

type ItemBalance struct {
	ID        uint64
	ProductId uint64
	Quantity  int
}
