package models

const (
	OperationShipment           = "shipment"
	OperationOrderReserve       = "order_reserve"
	OperationCancelOrderReserve = "order_reserve_cancel"
)

type ItemBalanceLog struct {
	ID        uint64
	ProductID uint64
	OrderID   uint64
	Operation string
	Quantity  int
}
