package storage

import (
	"errors"

	"shop/internal/common/events"
	orderEvents "shop/internal/common/events/order"
	"shop/internal/storage/domain/balance"
)

// StorageConfiguration is an alias for a function that will take in a pointer to an StorageService and modify it
type StorageConfiguration func(os *StorageService) error

func NewStorageService(cfgs ...StorageConfiguration) (StorageService, error) {
	s := StorageService{}
	// Apply all Configurations passed in
	for _, cfg := range cfgs {
		err := cfg(&s)
		if err != nil {
			return s, err
		}
	}

	if s.eventPublisher == nil {
		return s, errors.New("Storage service: service without event publisher")
	}

	// Подпишем наш сервис на события
	s.eventPublisher.Subscribe(NewReserveItemsHandler(&s, s.eventPublisher),
		orderEvents.Created{}, // Заказ создан
	)

	return s, nil
}

func WithStorageRepository(sr balance.Repository) StorageConfiguration {
	return func(s *StorageService) error {
		s.storage = sr
		return nil
	}
}

func WithEventPublisher(p *events.EventPublisher) StorageConfiguration {
	return func(s *StorageService) error {
		s.eventPublisher = p
		return nil
	}
}
