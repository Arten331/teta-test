package storage

import (
	"context"
	"go.opentelemetry.io/otel/attribute"

	"shop/internal/common/events"
	"shop/internal/common/events/order"
	"shop/internal/common/tracer"
	"shop/internal/storage/domain/balance"
)

// Пытаемся зарезервировать товары и публикуем новое событие по результату
type ReserveItemsHandler struct {
	storage   *StorageService
	publisher *events.EventPublisher
}

func NewReserveItemsHandler(s *StorageService, p *events.EventPublisher) *ReserveItemsHandler {
	return &ReserveItemsHandler{storage: s, publisher: p}
}

func (rih ReserveItemsHandler) Notify(ctx context.Context, event events.Event) {
	switch event.(type) {
	case order.Created:
		orderCreated := event.(order.Created)
		orderID := orderCreated.OrderID()
		orderItems := orderCreated.OrderItems

		ctx, span := tracer.NewSpan(ctx, "shop.storage.reserve-order-items")
		span.SetAttributes(attribute.Key("orderID").Int64(int64(orderCreated.OrderID())))
		defer span.End()

		err := rih.storage.ReserveOrderProducts(ctx, orderID, orderItems)
		if err != nil {
			rih.publisher.Notify(ctx, balance.NewOrderItemsReserveRejected(orderID))
			return
		}

		rih.publisher.Notify(ctx, balance.NewReservedInStorageSuccess(orderID))
	}
}
