package storage

import (
	"context"
	"errors"
	"go.uber.org/zap"

	"shop/internal/common/events"
	"shop/internal/common/events/order"
	"shop/internal/storage/domain/balance"
	"shop/internal/storage/models"
	"shop/pkg/logger"
)

type StorageService struct {
	storage        balance.Repository
	eventPublisher *events.EventPublisher
}

var ErrNotEnoughStockLeft = errors.New("not enough stock left")

func (s *StorageService) ReserveOrderProducts(
	ctx context.Context,
	orderID uint64,
	items []order.ReserveOrderItem,
) error {
	var err error

	productsBalances := make([]balance.ProductBalance, 0, len(items))
	balanceLog := make([]models.ItemBalanceLog, 0, len(items))

	for _, orderItem := range items {
		storageItem, err := s.storage.GetByItemID(ctx, orderItem.ItemID)
		if err != nil {
			return err
		}

		productsBalances = append(productsBalances, storageItem)

		if orderItem.Quantity > storageItem.Quantity() {
			return ErrNotEnoughStockLeft
		}

		balanceLog = append(balanceLog, models.ItemBalanceLog{
			ProductID: orderItem.ItemID,
			OrderID:   orderID,
			Operation: models.OperationOrderReserve,
			Quantity:  -orderItem.Quantity,
		})
	}

	err = s.storage.WriteOperations(ctx, balanceLog)
	if err != nil {
		logger.L().Error("error write balance log", zap.Error(err))

		return err
	}

	return nil
}

func (s *StorageService) CancelReserveOrderProducts(
	ctx context.Context,
	orderID uint64,
) error {
	balanceLogCanceled, err := s.storage.GetLogByOrderID(ctx, orderID)
	if err != nil {
		logger.L().Error("error get balance log for order", zap.Uint64("order_id", orderID), zap.Error(err))

		return err
	}

	balanceLog := make([]models.ItemBalanceLog, 0, len(balanceLogCanceled))

	for _, orderProductReserve := range balanceLogCanceled {
		balanceLog = append(balanceLog, models.ItemBalanceLog{
			ProductID: orderProductReserve.ProductID,
			OrderID:   orderID,
			Operation: models.OperationCancelOrderReserve,
			Quantity:  -orderProductReserve.Quantity,
		})
	}

	err = s.storage.WriteOperations(ctx, balanceLog)
	if err != nil {
		logger.L().Error("error write balance log", zap.Error(err))

		return err
	}

	return nil
}
