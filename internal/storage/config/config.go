package config

import (
	"github.com/joho/godotenv"

	"shop/internal/common/tools"
	"shop/pkg/logger"
)

type DBSettings struct {
	Host           string
	Port           int
	DBName         string
	User           string
	Password       string
	UseLocal       bool
	MaxConnections int
	MinConnection  int
	ConnTimeout    int
}

type QueueConfig struct {
	KafkaBroker *KafkaBroker
	Topics      *TopicsList
}

type (
	TopicsList struct {
		OrderEvents *ConsumeTopicConfig
	}
	KafkaBroker struct {
		Host string
		Port int
	}
	ProduceTopicConfig struct {
		Name string
	}
	ConsumeTopicConfig struct {
		Name  string
		Group string
	}
)

type DBConfig struct {
	Storage DBSettings
}

type AppConfig struct {
	ServiceName        string
	QueueService       *QueueConfig
	Logger             *logger.Options
	DB                 *DBConfig
	OTELTraceCollector string
}

func Init() (AppConfig, error) {
	var config AppConfig

	// default AppConfig
	config = AppConfig{
		DB: &DBConfig{},
	}

	err := godotenv.Load()
	if err != nil {
		return config, err
	}

	config.ServiceName = tools.GetEnvAsStr("STORAGE_SERVICE_NAME", "shop.storage")
	config.OTELTraceCollector = tools.GetEnvAsStr("OTEL_TRACE_COLLECTOR", "")

	config.Logger = &logger.Options{
		Level: tools.GetEnvAsStr("LOGGER_LEVEL", "INFO"),
		Debug: tools.GetEnvAsBool("LOGGER_DEBUG", false),
	}

	config.DB.Storage = DBSettings{
		Host:           tools.GetEnvAsStr("DB_POSTGRES_STORAGE_HOST", ""),
		Port:           tools.GetEnvAsInt("DB_POSTGRES_STORAGE_PORT", 0),
		DBName:         tools.GetEnvAsStr("DB_POSTGRES_STORAGE", ""),
		User:           tools.GetEnvAsStr("DB_POSTGRES_STORAGE_USER", ""),
		Password:       tools.GetEnvAsStr("DB_POSTGRES_STORAGE_PASSWORD", ""),
		MinConnection:  tools.GetEnvAsInt("DB_POSTGRES_STORAGE_MIN_CONNECTIONS", 0),
		MaxConnections: tools.GetEnvAsInt("DB_POSTGRES_STORAGE_MAX_CONNECTIONS", 40),
		ConnTimeout:    tools.GetEnvAsInt("DB_POSTGRES_STORAGE_TIMEOUT", 30),
	}

	config.QueueService = &QueueConfig{
		KafkaBroker: &KafkaBroker{
			Host: tools.GetEnvAsStr("KAFKA_BROKER_HOST", ""),
			Port: tools.GetEnvAsInt("KAFKA_BROKER_PORT", 0),
		},
		Topics: &TopicsList{
			OrderEvents: &ConsumeTopicConfig{
				Name:  tools.GetEnvAsStr("KAFKA_TOPIC_ORDER_EVENTS", ""),
				Group: tools.GetEnvAsStr("KAFKA_TOPIC_STORE_CONSUMER_GROUP", ""),
			},
		},
	}

	return config, nil
}
