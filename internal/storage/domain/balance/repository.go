package balance

import (
	"context"
	"errors"

	"shop/internal/storage/models"
)

var (
	ErrWriteBalanceLog        = errors.New("Error write balance log")
	ErrGetOrderBalanceLog     = errors.New("Error get order balance log")
	ErrGetProductBalance      = errors.New("Error get item balance")
	ErrProductBalanceNotFound = errors.New("item balance not found")
)

type Repository interface {
	GetByItemID(ctx context.Context, itemID uint64) (ProductBalance, error)
	WriteOperations(ctx context.Context, log []models.ItemBalanceLog) error
	GetLogByOrderID(ctx context.Context, orderID uint64) ([]models.ItemBalanceLog, error)
}
