package balance

import (
	"errors"

	"shop/internal/common/events/order"
	"shop/internal/storage/models"
)

var ErrMissingValues = errors.New("missing values")

type ProductBalance struct {
	product   models.ItemBalance
	log       []*models.ItemBalanceLog
	logLoaded bool
}

func NewProductBalance(productBalance models.ItemBalance, log []*models.ItemBalanceLog) (ProductBalance, error) {
	if productBalance.ID == 0 || productBalance.ProductId == 0 {
		return ProductBalance{}, ErrMissingValues
	}

	return ProductBalance{
		product:   productBalance,
		log:       log,
		logLoaded: log != nil,
	}, nil
}

func NewOrderItemsReserveRejected(orderID uint64) order.ReserveInStorageRejected {
	return order.ReserveInStorageRejected{
		ID: orderID,
	}
}

func NewReservedInStorageSuccess(orderID uint64) order.ReservedInStorage {
	return order.ReservedInStorage{
		ID: orderID,
	}
}

func (p *ProductBalance) LoadBalanceLog(log []*models.ItemBalanceLog) {
	p.log = log
	p.logLoaded = true
}

func (p ProductBalance) Quantity() int {
	return p.product.Quantity
}

func (p ProductBalance) Log() []*models.ItemBalanceLog {
	return p.log
}

func (p ProductBalance) LogLoaded() bool {
	return p.logLoaded
}
