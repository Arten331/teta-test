package postgres

const (
	queryGetItemBalance = `select ib.id, ib.product_id, ib.quantity from %[1]s ib where ib.product_id = $1 FOR UPDATE`
	queryInsertLog      = `insert into %[1]s (product_id, order_id, operation, quantity)
		values ($1, $2, $3, $4) returning id`
	queryChangeBalance        = `update %[1]s ib set quantity = ib.quantity + $2 where ib.product_id = $1;`
	queryGetOrderReservingLog = `select id, product_id ,order_id, operation, quantity from %[1]s 
where order_id = $1 and operation = '%[2]s';`
)
