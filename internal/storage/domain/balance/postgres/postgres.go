package postgres

import (
	"context"
	"fmt"
	"go.uber.org/zap"

	"shop/internal/common/database/repository/pgx"
	"shop/internal/storage/domain/balance"
	"shop/internal/storage/models"
	"shop/pkg/logger"
)

//  TODO: Environment
const (
	itemBalanceTable    = "storage.item_balance"
	itemBalanceLogTable = "storage.item_balance_log"
)

type ProductBalanceRepository struct {
	pgx.Repository
	queries Queries
}

type Queries struct {
	getItemBalance       string
	insertLog            string
	changeBalance        string
	getOrderReservingLog string
}

func (r *ProductBalanceRepository) GetByItemID(ctx context.Context, itemID uint64) (balance.ProductBalance, error) {
	var err error
	var b balance.ProductBalance

	rows, err := r.Conn().Query(ctx, r.queries.getItemBalance, itemID)
	if err != nil {
		logger.L().Error(balance.ErrGetProductBalance.Error(), zap.Error(err))

		return b, balance.ErrGetProductBalance
	}

	if !rows.Next() {
		return b, balance.ErrProductBalanceNotFound
	}

	b, err = r.fetchProductBalanceRow(rows)
	if err != nil {
		logger.L().Error(balance.ErrGetProductBalance.Error(), zap.Error(err))
		return b, balance.ErrGetProductBalance
	}
	rows.Close()

	return b, nil
}

func (r *ProductBalanceRepository) WriteOperations(ctx context.Context, operations []models.ItemBalanceLog) error {
	var err error
	var id uint64

	for i := range operations {
		row := r.Conn().QueryRow(ctx, r.queries.insertLog,
			operations[i].ProductID,
			operations[i].OrderID,
			operations[i].Operation,
			operations[i].Quantity,
		)

		err = row.Scan(&id)
		if err != nil {
			logger.L().Error(balance.ErrWriteBalanceLog.Error(), zap.Any("operations", operations), zap.Error(err))

			return balance.ErrWriteBalanceLog
		}

		commandTag, err := r.Conn().Exec(ctx, r.queries.changeBalance,
			operations[i].ProductID,
			operations[i].Quantity,
		)
		if err != nil || commandTag.RowsAffected() != 1 {
			logger.L().Error(balance.ErrWriteBalanceLog.Error(), zap.Any("operations", operations), zap.Error(err))

			return balance.ErrWriteBalanceLog
		}
	}

	return nil
}

func (r *ProductBalanceRepository) GetLogByOrderID(ctx context.Context, orderID uint64) ([]models.ItemBalanceLog, error) {
	rows, err := r.Conn().Query(ctx, r.queries.getOrderReservingLog, orderID)
	if err != nil {
		logger.L().Error(balance.ErrGetOrderBalanceLog.Error(), zap.Uint64("order_id", orderID), zap.Error(err))

		return nil, balance.ErrGetOrderBalanceLog
	}

	var balanceLog []models.ItemBalanceLog

	for rows.Next() {
		productLog, err := r.fetchBalanceLogRow(rows)
		if err != nil {
			return nil, err
		}

		balanceLog = append(balanceLog, productLog)
	}
	rows.Close()

	return nil, err
}

func NewProductBalanceRepository(cfgs ...pgx.RepositoryConfiguration) (ProductBalanceRepository, error) {
	r := ProductBalanceRepository{
		Repository: pgx.Repository{},
		queries: Queries{
			getItemBalance:       fmt.Sprintf(queryGetItemBalance, itemBalanceTable),
			insertLog:            fmt.Sprintf(queryInsertLog, itemBalanceLogTable),
			changeBalance:        fmt.Sprintf(queryChangeBalance, itemBalanceTable),
			getOrderReservingLog: fmt.Sprintf(queryGetOrderReservingLog, itemBalanceLogTable, models.OperationOrderReserve),
		},
	}

	for _, cfg := range cfgs {
		err := cfg(&r.Repository)
		if err != nil {
			return r, err
		}
	}

	return r, nil
}
