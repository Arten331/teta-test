package postgres

import (
	"errors"
	"go.uber.org/zap"

	"github.com/jackc/pgx/v4"

	"shop/internal/storage/domain/balance"
	"shop/internal/storage/models"
	"shop/pkg/logger"
)

var ErrFetchBalance = errors.New("unable fetch product balance")
var ErrFetchBalanceLog = errors.New("unable fetch balance log")

func (r ProductBalanceRepository) fetchProductBalanceRow(row pgx.Rows) (balance.ProductBalance, error) {
	var itemBalance models.ItemBalance

	err := row.Scan(
		&itemBalance.ID,
		&itemBalance.ProductId,
		&itemBalance.Quantity,
	)

	if err != nil {
		logger.L().Error(ErrFetchBalance.Error(), zap.Error(err))

		return balance.ProductBalance{}, ErrFetchBalance
	}

	// load without log
	productBalance, err := balance.NewProductBalance(itemBalance, nil)
	if err != nil {
		return balance.ProductBalance{}, err
	}

	return productBalance, err
}

func (r ProductBalanceRepository) fetchBalanceLogRow(row pgx.Rows) (models.ItemBalanceLog, error) {
	var itemBalanceLog models.ItemBalanceLog

	err := row.Scan(
		&itemBalanceLog.ID,
		&itemBalanceLog.ProductID,
		&itemBalanceLog.OrderID,
		&itemBalanceLog.Operation,
		&itemBalanceLog.Quantity,
	)

	if err != nil {
		logger.L().Error(ErrFetchBalanceLog.Error(), zap.Error(err))

		return models.ItemBalanceLog{}, ErrFetchBalanceLog
	}

	return itemBalanceLog, err
}
