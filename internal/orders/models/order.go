package models

const (
	StatusCreated         = "created"
	StatusOrderReserved   = "reserved"
	StatusReserveRejected = "reserve_rejected"
	StatusPaymentRejected = "payment_rejected"
	StatusPaymentSuccess  = "payment_success"
)

type Order struct {
	ID     uint64
	UserID uint64
	Status string
}

type OrderItem struct {
	ID       uint64 `json:"id,omitempty"`
	OrderID  uint64 `json:"-"`
	ItemID   uint64 `json:"item_id"`
	Quantity int    `json:"quantity,omitempty"`
	Price    int    `json:"price,omitempty"`
}

// value object - use for create order items
type ProductQuantity struct {
	ItemID   uint64 `json:"item_id"`
	Quantity int    `json:"quantity"`
}
