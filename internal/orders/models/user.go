package models

import "time"

type User struct {
	ID                 uint64    `json:"id"`
	Email              string    `json:"email"`
	LastOrderCreatedAt time.Time `json:"-"`
}
