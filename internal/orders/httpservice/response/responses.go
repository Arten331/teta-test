package response

import "shop/internal/orders/models"

// Use for swagger generator
type JSONResponseWithoutData struct {
	Message string `json:"message,omitempty" example:"Error or Success message if exist"`
}

// Use for swagger generator
type JSONResponseWithData struct {
	Message string        `json:"message,omitempty" example:"Error or Success message if exist"`
	Data    []interface{} `json:"data,omitempty" swaggertype:"object"`
}

type MeHandlerResponse struct {
	User *models.User `json:"user"`
}

type OrderResponse struct {
	OrderID    uint64              `json:"order_id,omitempty"`
	OrderItems []OrderResponseItem `json:"order_items,omitempty"`
	TotalPrice int                 `json:"total_price"`
	Status     string              `json:"status"`
}

// OrderResponseItem Позиция в заказе
type OrderResponseItem struct {
	ItemID   uint64 `json:"item_id,omitempty"`
	Quantity int    `json:"quantity,omitempty"`
	Price    int    `json:"price,omitempty"`
}
