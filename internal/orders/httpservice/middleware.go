package httpservice

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"go.opentelemetry.io/otel/attribute"
	"go.uber.org/zap"
	"net/http"

	chi "github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"

	"shop/internal/common/tracer"
	"shop/internal/orders/httpservice/request"
	"shop/pkg/logger"
	middlewareWrapper "shop/pkg/middlewares"
)

const (
	// KeyGroupBase For all requests
	KeyGroupBase     middlewareWrapper.MiddlewareGroupKey = "base"
	KeyGroupNeedAuth middlewareWrapper.MiddlewareGroupKey = "auth"
	KeyGroupPost     middlewareWrapper.MiddlewareGroupKey = "post"
)

type contextKey string

const (
	KeyContextUser        contextKey = "userInfo"
	KeyContextPostRequest contextKey = "postForm"
)

var (
	ErrParseJSONPost = errors.New("error while parse json from POST request")
	ErrBadRequest    = errors.New(http.StatusText(http.StatusBadRequest))
	ErrUnauthorized  = errors.New("Unauthorized")
)

func (s *HTTPService) configureMiddlewares() {
	groups := middlewareWrapper.MiddlewareGroups{
		KeyGroupBase: &chi.Middlewares{
			middleware.RequestID,
			middleware.RealIP,
			middleware.Logger,
			middleware.Recoverer,
			s.trace, // Setup CORS for front
		},
		KeyGroupNeedAuth: &chi.Middlewares{
			s.requireAuth,
		},
		KeyGroupPost: &chi.Middlewares{
			s.postFormHandler,
		},
	}

	mw := middlewareWrapper.NewMiddleWareWrapper(middlewareWrapper.Options{
		Groups: &groups,
	})

	s.middlewares = mw
}

func (s *HTTPService) requireAuth(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		form, ok := r.Context().Value(KeyContextPostRequest).(*request.PostForm)
		if !ok || form.UserForm == nil || form.UserForm.UserID == 0 {
			s.response.WriteError(w, ErrUnauthorized, http.StatusUnauthorized)

			return
		}

		u, err := s.services.order.GetUser(r.Context(), form.UserForm.UserID)

		if err != nil {
			logger.L().Error(ErrUnauthorized.Error(), zap.Error(err))

			s.response.WriteError(w, err, http.StatusUnauthorized)

			return
		}

		ctx := context.WithValue(r.Context(), KeyContextUser, u)
		next.ServeHTTP(w, r.WithContext(ctx))
	}

	return http.HandlerFunc(fn)
}

func (s *HTTPService) trace(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		ctx, span := tracer.NewSpan(r.Context(), fmt.Sprintf("shop.orders.http - %s", r.URL.Path))
		defer span.End()

		next.ServeHTTP(w, r.WithContext(ctx))
	}

	return http.HandlerFunc(fn)
}

func (s *HTTPService) postFormHandler(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		rPostForm := &request.PostForm{}
		// Bad practice 😄
		err := json.NewDecoder(r.Body).Decode(&rPostForm)
		if err != nil {
			logger.L().Error(ErrParseJSONPost.Error(), zap.Error(err))
			s.response.WriteError(w, ErrBadRequest, http.StatusBadRequest)

			return
		}

		span := tracer.SpanFromContext(r.Context())
		rawPostForm, _ := json.Marshal(rPostForm)
		span.SetAttributes(
			attribute.Key("form").String(string(rawPostForm)),
		)
		defer span.End()

		ctx := context.WithValue(r.Context(), KeyContextPostRequest, rPostForm)

		next.ServeHTTP(w, r.WithContext(ctx))
	}

	return http.HandlerFunc(fn)
}
