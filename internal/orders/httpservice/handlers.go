package httpservice

import (
	"errors"
	"net/http"
	"strconv"

	"gopkg.in/go-playground/validator.v9"

	"shop/internal/orders/httpservice/request"
	"shop/internal/orders/httpservice/response"
	"shop/internal/orders/models"
)

var ErrorUserNotFound = errors.New("UserForm not found")

// Me godoc
// @Summary      Me
// @Description  check authentication by user_id in post query
// @Tags         shop
// @Accept       json
// @Produce      json
// @Success      200  {object}  response.JSONResponseWithData{data=response.MeHandlerResponse} "success authentication, return user info with orders"
// @Failure      401  {object}  response.JSONResponseWithoutData "wrong user id in post"
// @Router       /me [post]
func (s *HTTPService) meHandler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		user, ok := r.Context().Value(KeyContextUser).(models.User)
		if !ok {
			s.response.WriteError(w, ErrorUserNotFound, http.StatusUnauthorized)

			return
		}

		responseData := response.MeHandlerResponse{
			User: &user,
		}

		s.response.WriteSuccess(w, "", responseData)
	}
}

// Create order with products godoc
// @Summary      CreateEmpty order with products
// @Tags         shop
// @Accept       json
// @Produce      json
// @Param        order   body      request.OrderWithProductsForm true "products list for new order"
// @Success      200     {object}  response.JSONResponseWithData{data=response.OrderResponse}
// @Failure      400     {object}  response.JSONResponseWithoutData "bad request"
// @Failure      401     {object}  response.JSONResponseWithoutData "wrong user id"
// @Failure      422     {object}  response.JSONResponseWithoutData "data validation fail"
// @Failure      500     {object}  response.JSONResponseWithoutData "internal error"
// @Router       /create-order [post]
func (s *HTTPService) createOrder() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		user, ok := r.Context().Value(KeyContextUser).(models.User)
		if !ok {
			s.response.WriteError(w, ErrorUserNotFound, http.StatusUnauthorized)

			return
		}

		postForm, ok := r.Context().Value(KeyContextPostRequest).(*request.PostForm)
		if !ok {
			s.response.WriteError(w, ErrBadRequest, http.StatusBadRequest)

			return
		}

		createOrderForm := postForm.OrderWithProductsForm

		validate := validator.New()

		err := validate.Struct(createOrderForm)
		if err != nil {
			s.response.WriteError(w, err, http.StatusUnprocessableEntity)

			return
		}

		orderItems := make([]models.ProductQuantity, 0, len(createOrderForm.Products))

		for _, product := range createOrderForm.Products {
			orderItems = append(orderItems, models.ProductQuantity{
				Quantity: product.Quantity,
				ItemID:   product.ItemID,
			})
		}

		newOrder, err := s.services.order.CreateOrderWithItems(r.Context(), user.ID, orderItems)
		if err != nil {
			s.response.WriteError(w, err, http.StatusInternalServerError)

			return
		}

		s.response.WriteSuccess(w, "", newOrder.ToResponseData())
	}
}

// GetOrder godoc
// @Summary      Get order
// @Tags         shop
// @Produce      json
// @Param        orderID    path    int     true    "order ID"
// @Success      200     {object}  response.JSONResponseWithData{data=response.OrderResponse}
// @Failure      400     {object}  response.JSONResponseWithoutData "bad request"
// @Failure      500     {object}  response.JSONResponseWithoutData "internal error"
// @Router       /order [get]
func (s *HTTPService) GetOrder() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		orderIDs, ok := r.URL.Query()["orderID"]

		if !ok || len(orderIDs[0]) < 1 {
			s.response.WriteError(w, errors.New("missing orderID in query"), http.StatusBadRequest)
			return
		}

		orderID, _ := strconv.Atoi(orderIDs[0])

		order, err := s.services.order.Get(r.Context(), uint64(orderID))
		if err != nil {
			s.response.WriteError(w, err, http.StatusInternalServerError)

			return
		}

		s.response.WriteSuccess(w, "", order.ToResponseData())
	}
}

// Healthcheck godoc
// @Summary      Healthcheck
// @Description  Healthcheck must return {
// @Description    "message": "OK"
// @Description  }
// @Tags         healthcheck
// @Produce      json
// @Success      200  {object}  response.JSONResponseWithoutData "{"message": "OK"}"
// @Router       /healthcheck [get]
func (s *HTTPService) healthCheck() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		s.response.WriteSuccess(w, "OK", nil)
	}
}
