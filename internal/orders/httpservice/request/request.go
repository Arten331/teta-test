package request

type UserForm struct {
	UserID uint64 `json:"user_id"`
}

type OrderWithProductsForm struct {
	Products []ProductWithQuantityForm `json:"products" validation:"required"`
}

type ProductWithQuantityForm struct {
	ItemID   uint64 `json:"item_id" validation:"required"`
	Quantity int    `json:"quantity" validation:"required"`
}

// PostForm This struct for all json Requests
type PostForm struct {
	*UserForm
	*OrderWithProductsForm
}
