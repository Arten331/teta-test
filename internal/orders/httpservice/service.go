package httpservice

import (
	"context"
	"errors"
	"fmt"
	httpSwagger "github.com/swaggo/http-swagger"
	"go.uber.org/zap"
	"net/http"
	"net/http/pprof"

	_ "shop/internal/orders/docs"
	"shop/internal/orders/orders"

	chi "github.com/go-chi/chi/v5"

	"shop/internal/orders/httpservice/response"
	"shop/pkg/logger"
	middlewareWrapper "shop/pkg/middlewares"
)

type Services struct {
	order *orders.OrderService
}

type HTTPService struct {
	server      *http.Server
	router      *chi.Mux
	response    response.ResponseWritter
	middlewares middlewareWrapper.MiddlewareWrapper
	services    Services
}

type HTTPServiceConfiguration func(s *HTTPService) error

func New(cfgs ...HTTPServiceConfiguration) (HTTPService, error) {
	service := HTTPService{}

	// Apply all Configurations passed in
	for _, cfg := range cfgs {
		err := cfg(&service)
		if err != nil {
			return service, err
		}
	}

	service.configureMiddlewares()
	service.configureRouter()

	return service, nil
}

func (s *HTTPService) Run(ctx context.Context, cancel context.CancelFunc) {
	go func() {
		logger.L().Info(fmt.Sprintf("Start http httpserver on %s!", s.server.Addr))

		err := s.server.ListenAndServe()
		if err != nil && !errors.Is(err, http.ErrServerClosed) {
			logger.L().Error("error serve httpserver", zap.Error(err))
			cancel()
		}
	}()

	<-ctx.Done()

	logger.L().Info(fmt.Sprintf("Shutdown http httpserver on %s!", s.server.Addr))

	err := s.server.Shutdown(context.Background())
	if err != nil {
		logger.L().Error("Unable shutdown http httpserver", zap.Error(err))
	}
}

func (s *HTTPService) Shutdown(shutdownCtx context.Context) error {
	return s.server.Shutdown(shutdownCtx)
}

func (s *HTTPService) configureRouter() {
	mwGroups := s.middlewares.Groups

	s.router.Use(mwGroups.GetChain(KeyGroupBase)...)

	s.router.Get("/swagger/*", httpSwagger.Handler())

	s.router.Route("/", func(r chi.Router) {
		r.Get("/healthcheck", s.healthCheck())

		r.Get("/order", s.GetOrder())

		r.Group(func(r chi.Router) {
			r.Use(mwGroups.GetChain(KeyGroupPost)...)
			r.Use(mwGroups.GetChain(KeyGroupNeedAuth)...)
			r.Post("/create-order", s.createOrder())
		})
	})

	// enable pprof
	s.router.With(mwGroups.GetChain(KeyGroupBase)...).Route("/debug", func(r chi.Router) {
		r.HandleFunc("/pprof", pprof.Index)
		r.HandleFunc("/pprof/cmdline", pprof.Cmdline)
		r.HandleFunc("/pprof/profile", pprof.Profile)
		r.HandleFunc("/pprof/symbol", pprof.Symbol)
		r.HandleFunc("/pprof/trace", pprof.Trace)
		r.Handle("/pprof/goroutine", pprof.Handler("goroutine"))
		r.Handle("/pprof/threadcreate", pprof.Handler("threadcreate"))
		r.Handle("/pprof/mutex", pprof.Handler("mutex"))
		r.Handle("/pprof/heap", pprof.Handler("heap"))
		r.Handle("/pprof/block", pprof.Handler("block"))
		r.Handle("/pprof/allocs", pprof.Handler("allocs"))
	})
}

func WithHttpAddress(address string) HTTPServiceConfiguration {
	return func(s *HTTPService) error {
		s.router = chi.NewRouter()
		s.server = &http.Server{
			Addr:    address,
			Handler: s.router,
		}
		return nil
	}
}

func WithResponseService(r response.ResponseWritter) HTTPServiceConfiguration {
	return func(s *HTTPService) error {
		s.response = r
		return nil
	}
}

func WithOrderService(os *orders.OrderService) HTTPServiceConfiguration {
	return func(s *HTTPService) error {
		s.services.order = os
		return nil
	}
}
