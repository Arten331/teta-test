package app

import (
	"context"
	"errors"
	"fmt"
	"github.com/segmentio/kafka-go"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracegrpc"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.7.0"
	"go.uber.org/zap"
	"strconv"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	mongoDB "shop/internal/common/database/mongo"
	"shop/internal/common/database/postgres"
	mongoRepo "shop/internal/common/database/repository/mongo_base"
	"shop/internal/common/database/repository/pgx"
	"shop/internal/common/events"
	orderEvents "shop/internal/common/events/order"
	"shop/internal/common/tracer"
	"shop/internal/orders/config"
	"shop/internal/orders/domain/order"
	postgresOrder "shop/internal/orders/domain/order/postgres"
	"shop/internal/orders/domain/users"
	"shop/internal/orders/domain/users/mongo"
	"shop/internal/orders/httpservice"
	"shop/internal/orders/httpservice/response"
	"shop/internal/orders/orders"
	"shop/pkg/collector"
	kafkaClient "shop/pkg/kafka"
	"shop/pkg/logger"
	"shop/pkg/workpool"
)

type Databases struct {
	registry postgres.DB
	users    mongoDB.DB
}

type Repositories struct {
	order order.Repository
	user  users.Repository
}

type Services struct {
	order           orders.OrderService
	httpService     httpservice.HTTPService
	eventsCollector collector.CommandCollector
}

type App struct {
	serviceName    string
	cfg            config.AppConfig
	databases      Databases
	repositories   Repositories
	services       Services
	tracerProvider *sdktrace.TracerProvider
	events         struct {
		publisher events.EventPublisher
		// kafkaHandler   events.KafkaEventHandler
		kafkaCollector events.KafkaEventCollector
	}
}

func Init(ctx context.Context, cfg config.AppConfig) (ac *App, err error) {
	ac = &App{
		serviceName: cfg.ServiceName,
		cfg:         cfg,
	}

	err = ac.initDatabases(ctx)
	if err != nil {
		return nil, err
	}

	err = ac.initRepositories()
	if err != nil {
		return nil, err
	}

	err = ac.initTracerProvider(ctx)
	if err != nil {
		return nil, err
	}

	ac.initEventsServices(ctx)

	err = ac.initServices(ctx)
	if err != nil {
		return nil, err
	}

	return ac, nil
}

func (a *App) initDatabases(ctx context.Context) error {
	registryDBConf := a.cfg.DB.Registry
	registryDB, err := postgres.NewPostgresDB(ctx, postgres.DBOptions{
		DBName:          registryDBConf.Name,
		Host:            registryDBConf.Host,
		Port:            registryDBConf.Port,
		User:            registryDBConf.User,
		Password:        registryDBConf.Password,
		ConnTimeout:     registryDBConf.ConnTimeout,
		PoolMaxConn:     registryDBConf.MaxConnections,
		PoolMinConn:     registryDBConf.MinConnection,
		PoolLazyConnect: true,
	})
	if err != nil {
		logger.L().Error("Error templateRepository DB configuration", zap.Error(err))
	}

	usersDBConf := a.cfg.DB.Users
	usersDB, err := mongoDB.NewMongoDB(ctx, mongoDB.Options{
		DBName:   usersDBConf.Name,
		Host:     usersDBConf.Host,
		Port:     usersDBConf.Port,
		User:     usersDBConf.User,
		Password: usersDBConf.Password,
	})

	a.databases = Databases{
		registry: registryDB,
		users:    usersDB,
	}

	return nil
}

func (a *App) initRepositories() error {
	orderRepository, err := postgresOrder.NewPostgresOrderRepository(
		pgx.WithDatabase(&a.databases.registry),
	)

	userRepository, err := mongo.NewMongoUserRepository(
		mongoRepo.WithDatabase(&a.databases.users),
	)

	if err != nil {
		return err
	}

	a.repositories = Repositories{
		order: &orderRepository,
		user:  &userRepository,
	}

	return nil
}

func (a *App) initServices(_ context.Context) error {
	orderService, err := orders.NewOrderService(
		orders.WithOrderRepository(a.repositories.order),
		orders.WithUserRepository(a.repositories.user),
		orders.WithEventPublisher(&a.events.publisher),
	)
	if err != nil {
		return err
	}

	commandCollector, err := collector.NewCommandCollector(
		collector.WithCollectors([]collector.Collector{&a.events.kafkaCollector}),
	)
	if err != nil {
		return err
	}

	rw := response.NewJSONResponseWriter()

	httpService, err := httpservice.New(
		httpservice.WithHttpAddress(":"+strconv.Itoa(a.cfg.HTTPService.Port)),
		httpservice.WithResponseService(&rw),
		httpservice.WithOrderService(&orderService),
	)
	if err != nil {
		return err
	}

	a.services = Services{
		order:           orderService,
		httpService:     httpService,
		eventsCollector: commandCollector,
	}

	return err
}

func (a *App) initEventsServices(_ context.Context) {
	brokers := []string{
		fmt.Sprintf("%s:%d", a.cfg.QueueService.KafkaBroker.Host, a.cfg.QueueService.KafkaBroker.Port),
	}

	cfgQueue := a.cfg.QueueService

	a.events.publisher = events.NewEventPublisher()

	eventSender := events.NewKafkaEventHandler(kafkaClient.MustCreateProducer(kafkaClient.ProducerClientOptions{
		Brokers:    brokers,
		Topic:      cfgQueue.Topics.OrderEvents.Name,
		BeforeSend: tracer.InjectSpanToKafkaMessages,
	}))
	// Событие заказ создан публикуем
	a.events.publisher.Subscribe(eventSender,
		orderEvents.Created{},
	)

	orderEventConsumer := kafkaClient.MustCreateConsumer(kafkaClient.ConsumerClientOptions{
		Brokers: brokers,
		Topic:   cfgQueue.Topics.OrderEvents.Name,
		Group:   cfgQueue.Topics.OrderEvents.Group,
	})

	orderCheckedEvents := events.WithCheckedEvents(func(message kafka.Message) events.QueueableEvent {
		var event events.QueueableEvent

		//  Список ключей и события которые пытаемся обработать с очереди
		switch string(message.Key) {
		case orderEvents.KeyOrderItemsReservedInStorage:
			event = orderEvents.ReservedInStorage{}
		case orderEvents.KeyOrderItemsReserveRejected:
			event = orderEvents.ReserveInStorageRejected{}
		case orderEvents.KeyOrderPaymentSuccess:
			event = orderEvents.PaymentSuccess{}
		case orderEvents.KeyOrderPaymentRejected:
			event = orderEvents.PaymentRejected{}
		}

		return event
	})

	a.events.kafkaCollector, _ = events.NewOrderKafkaEventCollector(
		orderEventConsumer,
		&a.events.publisher,
		orderCheckedEvents,
	)
}

func (a *App) initTracerProvider(ctx context.Context) error {
	res, err := resource.New(ctx,
		resource.WithAttributes(
			semconv.ServiceNameKey.String(a.cfg.ServiceName),
		),
	)
	if err != nil {
		return err
	}

	conn, err := grpc.DialContext(
		ctx,
		a.cfg.OTELTraceCollector,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithBlock(),
	)
	if err != nil {
		return errors.New("failed to create gRPC connection to collector")
	}

	// Set up a trace exporter
	traceExporter, err := otlptracegrpc.New(ctx, otlptracegrpc.WithGRPCConn(conn))
	if err != nil {
		return errors.New("failed to create trace exporter")
	}

	// Register the trace exporter with a TracerProvider, using a batch
	// span processor to aggregate spans before export.
	bsp := sdktrace.NewBatchSpanProcessor(traceExporter)
	tracerProvider := sdktrace.NewTracerProvider(
		sdktrace.WithSampler(sdktrace.AlwaysSample()),
		sdktrace.WithResource(res),
		sdktrace.WithSpanProcessor(bsp),
	)
	otel.SetTracerProvider(tracerProvider)

	a.tracerProvider = tracerProvider

	// set global propagator to tracecontext (the default is no-op).
	otel.SetTextMapPropagator(propagation.NewCompositeTextMapPropagator(propagation.TraceContext{}, propagation.Baggage{}))

	// setup global tracer for service
	tracer.SetupGlobalTracer(otel.Tracer(a.cfg.ServiceName), a.cfg.ServiceName)

	return nil
}

func (a *App) Run(ctx context.Context, cancelFunc context.CancelFunc) error {
	// command kafkaCollector service
	collectorWorkerPool := workpool.NewWorkerPool(workpool.Options{
		MaxWorkers: 0,
		CancelFunc: cancelFunc,
	})

	err := collector.WithWorker(collectorWorkerPool.Run(ctx))(&a.services.eventsCollector)
	if err != nil {
		return err
	}

	go a.services.httpService.Run(ctx, cancelFunc)
	go a.services.eventsCollector.Run(ctx, cancelFunc)

	return nil
}

func (a *App) Shutdown(ctx context.Context) error {
	var err error

	err = a.services.httpService.Shutdown(ctx)
	if err != nil {
		return err
	}

	err = a.tracerProvider.Shutdown(ctx)
	if err != nil {
		return err
	}

	return err
}
