package config

import (
	"github.com/joho/godotenv"

	"shop/internal/common/tools"
	"shop/pkg/logger"
)

type PostgresDBSettings struct {
	Host           string
	Port           int
	Name           string
	User           string
	Password       string
	MaxConnections int
	MinConnection  int
	ConnTimeout    int
}

type MongoDBSettings struct {
	Host     string
	Port     int
	Name     string
	User     string
	Password string
}

type QueueConfig struct {
	KafkaBroker *KafkaBroker
	Topics      *TopicsList
}

type (
	TopicsList struct {
		OrderEvents *ConsumeTopicConfig
	}
	KafkaBroker struct {
		Host string
		Port int
	}
	ProduceTopicConfig struct {
		Name string
	}
	ConsumeTopicConfig struct {
		Name  string
		Group string
	}
)

type HTTPServiceConfig struct {
	Env         string
	Port        int
	FrontOrigin string
}

type DBConfig struct {
	Registry PostgresDBSettings
	Users    MongoDBSettings
}

type AppConfig struct {
	ServiceName        string
	HTTPService        *HTTPServiceConfig
	QueueService       *QueueConfig
	Logger             *logger.Options
	DB                 *DBConfig
	OTELTraceCollector string
}

func Init() (AppConfig, error) {
	var config AppConfig

	err := godotenv.Load()
	if err != nil {
		return config, err
	}

	// default AppConfig
	config = AppConfig{
		DB: &DBConfig{},
	}

	config.ServiceName = tools.GetEnvAsStr("ORDERS_SERVICE_NAME", "shop.orders")
	config.OTELTraceCollector = tools.GetEnvAsStr("OTEL_TRACE_COLLECTOR", "")

	config.HTTPService = &HTTPServiceConfig{
		Env:         tools.GetEnvAsStr("env", ""),
		Port:        tools.GetEnvAsInt("HTTP_PORT", 8081),
		FrontOrigin: tools.GetEnvAsStr("FRONT_ORIGIN", ""),
	}

	config.Logger = &logger.Options{
		Level: tools.GetEnvAsStr("LOGGER_LEVEL", "INFO"),
		Debug: tools.GetEnvAsBool("LOGGER_DEBUG", false),
	}

	config.DB.Users = MongoDBSettings{
		Host:     tools.GetEnvAsStr("DB_MONGO_USERS_HOST", ""),
		Port:     tools.GetEnvAsInt("DB_MONGO_USERS_PORT", 30001),
		Name:     tools.GetEnvAsStr("DB_MONGO_USERS_NAME", ""),
		User:     tools.GetEnvAsStr("DB_MONGO_USERS_USER", ""),
		Password: tools.GetEnvAsStr("DB_MONGO_USERS_PASS", ""),
	}

	config.DB.Registry = PostgresDBSettings{
		Host:           tools.GetEnvAsStr("DB_POSTGRES_REGISTRY_HOST", ""),
		Port:           tools.GetEnvAsInt("DB_POSTGRES_REGISTRY_PORT", 0),
		Name:           tools.GetEnvAsStr("DB_POSTGRES_REGISTRY", ""),
		User:           tools.GetEnvAsStr("DB_POSTGRES_REGISTRY_USER", ""),
		Password:       tools.GetEnvAsStr("DB_POSTGRES_REGISTRY_PASSWORD", ""),
		MinConnection:  tools.GetEnvAsInt("DB_POSTGRES_REGISTRY_MIN_CONNECTIONS", 0),
		MaxConnections: tools.GetEnvAsInt("DB_POSTGRES_REGISTRY_MAX_CONNECTIONS", 40),
		ConnTimeout:    tools.GetEnvAsInt("DB_POSTGRES_REGISTRY_TIMEOUT", 30),
	}

	config.QueueService = &QueueConfig{
		KafkaBroker: &KafkaBroker{
			Host: tools.GetEnvAsStr("KAFKA_BROKER_HOST", ""),
			Port: tools.GetEnvAsInt("KAFKA_BROKER_PORT", 0),
		},
		Topics: &TopicsList{
			OrderEvents: &ConsumeTopicConfig{
				Name:  tools.GetEnvAsStr("KAFKA_TOPIC_ORDER_EVENTS", ""),
				Group: tools.GetEnvAsStr("KAFKA_TOPIC_REGISTRY_CONSUMER_GROUP", ""),
			},
		},
	}

	return config, nil
}
