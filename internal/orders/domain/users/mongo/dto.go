package mongo

import (
	"time"

	"shop/internal/orders/models"
)

type DBUserModel struct {
	ID                 uint64    `bson:"id"`
	Email              string    `bson:"email"`
	LastOrderCreatedAt time.Time `bson:"last_order_created"`
}

func (u DBUserModel) ToModel() models.User {
	return models.User{
		// ID:                 u.id,
		Email:              u.Email,
		LastOrderCreatedAt: u.LastOrderCreatedAt,
	}
}

func FromModel(user models.User) DBUserModel {
	return DBUserModel{
		// id:                 user.ID,
		Email:              user.Email,
		LastOrderCreatedAt: user.LastOrderCreatedAt,
	}
}
