package mongo

import (
	"context"
	"errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"

	mongoBaseRepo "shop/internal/common/database/repository/mongo_base"
	"shop/internal/orders/domain/users"
	"shop/internal/orders/models"
	"shop/pkg/logger"
)

const collection = "users"

type UserRepository struct {
	mongoBaseRepo.Repository
	collection *mongo.Collection
}

func (u UserRepository) GetByID(ctx context.Context, id uint64) (user models.User, err error) {
	result, err := u.Conn().FindOne(ctx, u.collection, bson.D{
		{"id", bson.M{"$eq": id}},
	})

	if err != nil {
		if errors.Is(err, mongo.ErrNoDocuments) {
			logger.L().Error(users.ErrUserNotFound.Error(), zap.Uint64("user_id", id))

			return user, users.ErrUserNotFound
		} else {
			logger.L().Error(users.ErrUserNotFound.Error(), zap.Error(err), zap.Uint64("user_id", id))
		}

		return user, err
	}

	dbUser := &DBUserModel{}

	err = result.Decode(dbUser)
	if err != nil {
		return user, err
	}

	return dbUser.ToModel(), nil
}

func (u UserRepository) Update(ctx context.Context, user models.User) error {
	dbUser := FromModel(user)

	_, err := u.Conn().UpdateOne(
		ctx,
		u.collection,
		bson.D{
			{"id", bson.M{"$eq": user.ID}},
		},
		bson.M{
			"$set": dbUser,
		},
	)

	return err
}

func NewMongoUserRepository(cfgs ...mongoBaseRepo.RepositoryConfiguration) (UserRepository, error) {
	r := UserRepository{}

	for _, cfg := range cfgs {

		err := cfg(&r.Repository)
		if err != nil {
			return r, err
		}
	}

	coll, err := r.Conn().Collection(collection)
	if err != nil {
		return r, err
	}

	r.collection = coll

	return r, nil
}
