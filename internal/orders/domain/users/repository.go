package users

import (
	"context"
	"errors"

	"shop/internal/common/database"
	"shop/internal/orders/models"
)

var ErrUserNotFound = errors.New("user is not found")

type Repository interface {
	GetByID(ctx context.Context, id uint64) (models.User, error)
	Update(ctx context.Context, user models.User) error
	database.TXRepository
}
