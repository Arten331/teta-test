package users

import (
	"shop/internal/orders/models"
)

type User struct {
	user models.User
}

func NewUser(user models.User) (User, error) {
	return User{user: user}, nil
}
