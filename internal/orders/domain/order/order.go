package order

import (
	"errors"

	"shop/internal/common/events/order"
	"shop/internal/orders/httpservice/response"
	"shop/internal/orders/models"
)

type Order struct {
	order         *models.Order
	orderProducts []models.OrderItem
}

var ErrOrderMissingValues = errors.New("missing values")

func NewOrder(orderModel *models.Order, orderItems []models.OrderItem) (Order, error) {
	if orderModel == nil {
		return Order{}, ErrOrderMissingValues
	}

	o := Order{
		order:         orderModel,
		orderProducts: orderItems,
	}

	return o, nil
}

func NewOrderItemsFromProductsQuantity(quantity []models.ProductQuantity, products []models.Product, orderID uint64) ([]models.OrderItem, error) {
	// products map by id
	productsMap := make(map[uint64]*models.Product)
	for i := range products {
		productsMap[products[i].ID] = &products[i]
	}

	// fill order by products
	orderItems := make([]models.OrderItem, 0, len(quantity))
	for _, itemQuantity := range quantity {

		// get info about purchaseItem(product)
		item, ok := productsMap[itemQuantity.ItemID]
		if !ok {
			return nil, errors.New("Not found product")
		}

		purchaseItem := models.OrderItem{
			OrderID:  orderID,
			ItemID:   item.ID,
			Price:    item.Price,
			Quantity: itemQuantity.Quantity,
		}

		orderItems = append(orderItems, purchaseItem)
	}
	return orderItems, nil
}

func (o Order) ID() uint64 {
	return o.order.ID
}

func (o Order) Status() string {
	return o.order.Status
}

func (o Order) CustomerID() uint64 {
	return o.order.UserID
}

func (o Order) TotalPrice() int {
	totalPrice := 0
	for _, orderProduct := range o.GetOrderProducts() {
		totalPrice += orderProduct.Price
	}

	return totalPrice
}

func (o Order) GetOrderProducts() []models.OrderItem {
	return o.orderProducts
}

func (o Order) NewOrderCreatedEvent() order.Created {
	oItems := o.GetOrderProducts()

	eventOrderItems := make([]order.ReserveOrderItem, 0, len(oItems))
	for _, item := range oItems {
		eventOrderItems = append(eventOrderItems, order.ReserveOrderItem{
			ItemID:   item.ItemID,
			Quantity: item.Quantity,
		})
	}

	return order.Created{
		ID:         o.ID(),
		OrderItems: eventOrderItems,
	}
}

func (o Order) ToResponseData() response.OrderResponse {
	createOrderResponse := response.OrderResponse{
		OrderID:    o.ID(),
		OrderItems: make([]response.OrderResponseItem, 0, len(o.GetOrderProducts())),
		TotalPrice: 0,
		Status:     o.Status(),
	}

	for _, orderProduct := range o.GetOrderProducts() {
		createOrderResponse.OrderItems = append(createOrderResponse.OrderItems, response.OrderResponseItem{
			ItemID:   orderProduct.ItemID,
			Quantity: orderProduct.Quantity,
			Price:    orderProduct.Price,
		})
		createOrderResponse.TotalPrice += orderProduct.Price
	}

	return createOrderResponse
}
