package postgres

import (
	"context"
	"fmt"
	"go.uber.org/zap"

	"shop/internal/common/database/repository/pgx"
	"shop/internal/orders/domain/order"
	"shop/internal/orders/models"
	"shop/pkg/logger"
)

type Queries struct {
	createOrder       string
	getOrder          string
	getOrderItems     string
	updateOrderItem   string
	updateOrderStatus string
	getProductByID    string
}

type OrderRepository struct {
	pgx.Repository
	queries Queries
}

func (o *OrderRepository) GetByID(ctx context.Context, id uint64) (order.Order, error) {
	rows, err := o.Conn().Query(ctx, o.queries.getOrder, id)
	if err != nil {
		return order.Order{}, order.ErrGetOrder
	}

	if !rows.Next() {
		return order.Order{}, order.ErrOrderNotFound
	}

	orderEnt, err := o.fetchOrderRow(rows)
	if err != nil {
		return order.Order{}, order.ErrGetOrder
	}
	rows.Close()

	orderItems, err := o.GetItems(ctx, orderEnt.ID)
	if err != nil {
		return order.Order{}, err
	}

	orderAgr, err := order.NewOrder(&orderEnt, orderItems)
	if err != nil {
		logger.L().Error(order.ErrGetOrder.Error(), zap.Error(err))

		return order.Order{}, err
	}

	return orderAgr, nil
}

func (o *OrderRepository) Create(ctx context.Context, orderEntity models.Order) (uint64, error) {
	var orderID uint64

	rows, err := o.Conn().Query(ctx, o.queries.createOrder, orderEntity.UserID, models.StatusCreated)
	if err != nil || !rows.Next() {
		return orderID, order.ErrCreateOrder
	}

	orderID, err = o.fetchOrderID(rows)
	if err != nil {
		return orderID, order.ErrCreateOrder
	}
	rows.Close()

	return orderID, nil
}

func (o *OrderRepository) UpdateItems(ctx context.Context, purchaseItems []models.OrderItem) error {
	for _, purchaseItem := range purchaseItems {
		row := o.Conn().QueryRow(ctx, o.queries.updateOrderItem,
			purchaseItem.ID,
			purchaseItem.OrderID,
			purchaseItem.ItemID,
			purchaseItem.Quantity,
			purchaseItem.Price,
		)

		var id uint64

		err := row.Scan(&id)
		if err != nil || id == 0 {
			return order.ErrUpdateOrderItems
		}
	}

	return nil
}

func (o *OrderRepository) GetItems(ctx context.Context, orderID uint64) ([]models.OrderItem, error) {
	rows, err := o.Conn().Query(ctx, o.queries.getOrderItems, orderID)
	if err != nil {
		return nil, order.ErrGetOrderItems
	}

	var orderItems = make([]models.OrderItem, 0, 0)

	for rows.Next() {
		orderItem, err := o.fetchOrderItem(rows)
		if err != nil {
			return nil, order.ErrGetOrderItems
		}

		orderItems = append(orderItems, orderItem)
	}

	return orderItems, nil
}

func (o *OrderRepository) UpdateStatus(ctx context.Context, orderID uint64, status string) error {
	commandTag, err := o.Conn().Exec(ctx, o.queries.updateOrderStatus, orderID, status)
	if err != nil {
		return order.ErrUpdateStatus
	}

	if commandTag.RowsAffected() != 1 {
		return order.ErrOrderNotFound
	}

	return nil
}

func (o *OrderRepository) GetProductByID(ctx context.Context, itemID uint64) (models.Product, error) {
	rows, err := o.Conn().Query(ctx, o.queries.getProductByID, itemID)
	if err != nil {
		return models.Product{}, order.ErrGetProduct
	}

	if !rows.Next() {
		return models.Product{}, order.ErrProductNotFound
	}

	item, err := o.fetchProductRow(rows)
	if err != nil {
		return models.Product{}, order.ErrGetProduct
	}
	rows.Close()

	return item, nil
}

func NewPostgresOrderRepository(cfgs ...pgx.RepositoryConfiguration) (OrderRepository, error) {
	r := OrderRepository{
		Repository: pgx.Repository{},
		queries: Queries{
			createOrder:       fmt.Sprintf(queryCreateOrder, orderTable),
			getOrder:          fmt.Sprintf(queryGetOrder, orderTable),
			getOrderItems:     fmt.Sprintf(queryGetOrderItems, purchaseItemsTable),
			updateOrderItem:   fmt.Sprintf(queryWritePurchaseItem, purchaseItemsTable),
			updateOrderStatus: fmt.Sprintf(queryUpdateOrderStatus, orderTable),
			getProductByID:    fmt.Sprintf(queryGetProduct, itemsTable),
		},
	}

	for _, cfg := range cfgs {

		err := cfg(&r.Repository)
		if err != nil {
			return r, err
		}
	}

	return r, nil
}
