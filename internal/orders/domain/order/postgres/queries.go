package postgres

const (
	orderTable         = "shop.order"
	purchaseItemsTable = "shop.purchase_item"
	itemsTable         = "shop.item"
)

const (
	queryGetOrder      = `select id, user_id, status from %[1]s where id = $1;`
	queryCreateOrder   = `insert into %[1]s (user_id, status) select $1,$2 returning id;`
	queryGetOrderItems = `select pi.id, pi.price, pi.order_id, pi.item_id, pi.quantity
	from %[1]s pi where pi.order_id = $1;`
	queryUpdateOrderStatus = `update %[1]s set status = $2 where id = $1;`
	// Такой себе запрос) Но для учебного проекта такой upsert сойдет
	queryWritePurchaseItem = `with old_purchase_item AS (
	    update %[1]s set price = $5, quantity = $4 where id = $1 RETURNING id
	),
	new_purchase_item AS (
         INSERT INTO %[1]s (order_id, item_id, quantity, price) 
	             SELECT $2, $3, $4, $5 where NOT EXISTS(select id from %[1]s pi where pi.id = $1)
	             RETURNING id
    ) select id from old_purchase_item union select id from new_purchase_item;`
	queryGetProduct  = `select i.id, i.name, i.price from %[1]s i where i.id = $1`
	queryGetProducts = `select i.id, i.name, i.price from %[1]s i where i.id in ($1)`
)
