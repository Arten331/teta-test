package postgres

import (
	"errors"
	"go.uber.org/zap"

	"github.com/jackc/pgx/v4"

	"shop/internal/orders/models"
	"shop/pkg/logger"
)

var (
	ErrUnableFetchOrder     = errors.New("unable fetch order")
	ErrUnableFetchOrderItem = errors.New("unable fetch order item")
	ErrUnableFetchOrderID   = errors.New("unable fetch order ID")
	ErrUnableFetchProduct   = errors.New("Unable fetch product")
)

func (o OrderRepository) fetchOrderRow(row pgx.Rows) (models.Order, error) {

	order := models.Order{}

	err := row.Scan(
		&order.ID,
		&order.UserID,
		&order.Status,
	)

	if err != nil {
		logger.L().Error(ErrUnableFetchOrder.Error(), zap.Error(err))

		return models.Order{}, ErrUnableFetchOrder
	}

	return order, err
}

func (o OrderRepository) fetchOrderID(row pgx.Rows) (uint64, error) {

	var orderID uint64

	err := row.Scan(
		&orderID,
	)

	if err != nil {
		logger.L().Error(ErrUnableFetchOrder.Error(), zap.Error(err))

		return 0, ErrUnableFetchOrderID
	}

	return orderID, err
}

func (o OrderRepository) fetchOrderItem(row pgx.Rows) (models.OrderItem, error) {

	orderItem := models.OrderItem{}

	err := row.Scan(
		&orderItem.ID,
		&orderItem.Price,
		&orderItem.OrderID,
		&orderItem.ItemID,
		&orderItem.Quantity,
	)

	if err != nil {
		logger.L().Error(ErrUnableFetchOrderItem.Error(), zap.Error(err))

		return models.OrderItem{}, ErrUnableFetchOrderItem
	}

	return orderItem, err
}

func (o OrderRepository) fetchProductRow(row pgx.Rows) (models.Product, error) {

	product := models.Product{}

	err := row.Scan(
		&product.ID,
		&product.Name,
		&product.Price,
	)

	if err != nil {
		logger.L().Error(ErrUnableFetchProduct.Error(), zap.Error(err))

		return models.Product{}, ErrUnableFetchProduct
	}

	return product, err
}
