package order

import (
	"context"
	"errors"

	"shop/internal/common/database"
	"shop/internal/orders/models"
)

var (
	ErrOrderNotFound    = errors.New("order not found")
	ErrGetOrder         = errors.New("unexpected error while fetch order")
	ErrCreateOrder      = errors.New("error create order")
	ErrGetOrderItems    = errors.New("error while getting list of order items")
	ErrUpdateStatus     = errors.New("error try update order status")
	ErrUpdateOrderItems = errors.New("Unable update order items")
	ErrGetProduct       = errors.New("Unable get product")
	ErrProductNotFound  = errors.New("Product not found")
)

// Repository is the repository interface to fulfill to use the product aggregate
type Repository interface {
	GetByID(ctx context.Context, id uint64) (Order, error)
	Create(ctx context.Context, orderEntity models.Order) (uint64, error)
	UpdateStatus(ctx context.Context, orderID uint64, status string) error
	GetItems(ctx context.Context, orderID uint64) ([]models.OrderItem, error)
	UpdateItems(ctx context.Context, purchaseItems []models.OrderItem) error
	GetProductByID(ctx context.Context, itemID uint64) (models.Product, error)
	database.TXRepository
}
