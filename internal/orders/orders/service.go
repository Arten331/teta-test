package orders

import (
	"context"
	"fmt"
	"time"

	"shop/internal/common/database"
	"shop/internal/common/events"
	"shop/internal/common/tracer"
	"shop/internal/orders/domain/order"
	"shop/internal/orders/domain/users"
	"shop/internal/orders/models"
)

type Repositories struct {
	order order.Repository
	user  users.Repository
}

type OrderService struct {
	repositories   Repositories
	eventPublisher *events.EventPublisher
}

func (s OrderService) CreateOrderWithItems(ctx context.Context, customerID uint64, itemsQuantity []models.ProductQuantity) (order.Order, error) {
	var err error
	var newOrder order.Order

	ctx, span := tracer.NewSpan(ctx, fmt.Sprintf("%s.order.create", tracer.GetServiceName()))
	defer span.End()

	// Тестовый/Пробный кусок кода - тестирую обертку TX manager для работы с разными репозиториями!!!
	// Пока даже работает)
	txManager := database.NewTXManager()
	orderRepository := s.repositories.order
	userRepository := s.repositories.user

	err = txManager.RunInTransaction(
		ctx,
		func(ctx context.Context) error {
			var err error

			// get requested products from DB
			products := make([]models.Product, 0, len(itemsQuantity))
			for i := range itemsQuantity {
				product, err := orderRepository.GetProductByID(ctx, itemsQuantity[i].ItemID)
				if err != nil {
					if err == order.ErrProductNotFound {
						return ErrRequestedProductNotFound(itemsQuantity[i].ItemID)
					}
					return err
				}
				products = append(products, product)
			}

			// create new order - empty
			newOrderID, err := orderRepository.Create(ctx, models.Order{
				UserID: customerID,
			})
			if err != nil {
				return err
			}

			// create order items from
			orderItems, err := order.NewOrderItemsFromProductsQuantity(itemsQuantity, products, newOrderID)
			if err != nil {
				return err
			}

			// write purchase items in db
			err = orderRepository.UpdateItems(ctx, orderItems)
			if err != nil {
				return err
			}

			// get updated order
			newOrder, err = orderRepository.GetByID(ctx, newOrderID)
			if err != nil {
				return err
			}

			// update user
			user, _ := s.GetUser(ctx, customerID)
			user.LastOrderCreatedAt = time.Now()
			err = userRepository.Update(ctx, user)
			if err != nil {
				return err
			}

			return nil
		},
		userRepository,
		orderRepository,
	)
	if err != nil {
		return order.Order{}, err
	}

	// notify event
	s.eventPublisher.Notify(ctx, newOrder.NewOrderCreatedEvent())

	return newOrder, nil
}

func (s OrderService) UpdateStatus(ctx context.Context, orderID uint64, status string) error {

	ctx, span := tracer.NewSpan(ctx, fmt.Sprintf("%s.order.update-status", tracer.GetServiceName()))
	defer span.End()

	err := s.repositories.order.UpdateStatus(ctx, orderID, status)
	if err != nil {
		return err
	}

	return nil
}

func (s OrderService) GetUser(ctx context.Context, id uint64) (models.User, error) {
	return s.repositories.user.GetByID(ctx, id)
}

func (s OrderService) Get(ctx context.Context, id uint64) (order.Order, error) {
	return s.repositories.order.GetByID(ctx, id)
}
