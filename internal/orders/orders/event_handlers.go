package orders

import (
	"context"
	"errors"
	"fmt"
	"go.opentelemetry.io/otel/attribute"
	"go.uber.org/zap"

	"shop/internal/common/events"
	orderEvents "shop/internal/common/events/order"
	"shop/internal/common/tracer"
	"shop/internal/orders/models"
	"shop/pkg/logger"
)

var errTypeAssertionHandler = func(eventName string) error {
	return errors.New(fmt.Sprintf("Unable notify %[1]s event, type assertion error", eventName))
}

type OrderStatusChangedHandler struct {
	orderService *OrderService
}

func NewOrderStatusChangedHandler(orderService *OrderService) OrderStatusChangedHandler {
	return OrderStatusChangedHandler{
		orderService: orderService,
	}
}

func (h OrderStatusChangedHandler) Notify(ctx context.Context, event events.Event) {
	switch event.(type) {
	case orderEvents.ReservedInStorage:
		e, _ := event.(orderEvents.ReservedInStorage)

		ctx, span := tracer.NewSpanWithAttributes(
			ctx,
			"shop.orders.reserve-order-items-success",
			attribute.Key("orderID").Int64(int64(e.OrderID())),
		)
		defer span.End()

		h.updateStatus(ctx, e, models.StatusOrderReserved)

	case orderEvents.ReserveInStorageRejected:
		e, _ := event.(orderEvents.ReserveInStorageRejected)

		ctx, span := tracer.NewSpanWithAttributes(
			ctx,
			"shop.orders.reserve-order-items-rejected",
			attribute.Key("orderID").Int64(int64(e.OrderID())),
		)
		defer span.End()

		h.updateStatus(ctx, e, models.StatusReserveRejected)
	case orderEvents.PaymentSuccess:
		e, _ := event.(orderEvents.PaymentSuccess)

		ctx, span := tracer.NewSpanWithAttributes(
			ctx,
			"shop.orders.payment-success",
			attribute.Key("orderID").Int64(int64(e.OrderID())),
		)
		defer span.End()

		h.updateStatus(ctx, e, models.StatusPaymentSuccess)
	case orderEvents.PaymentRejected:
		e, _ := event.(orderEvents.PaymentRejected)

		ctx, span := tracer.NewSpanWithAttributes(
			ctx,
			"shop.orders.payment-rejected",
			attribute.Key("orderID").Int64(int64(e.OrderID())),
		)
		defer span.End()

		h.updateStatus(ctx, e, models.StatusPaymentRejected)
	default:
		logger.L().Error(errTypeAssertionHandler(event.Name()).Error())
	}
}

func (h OrderStatusChangedHandler) updateStatus(ctx context.Context, event orderEvents.Event, status string) {
	err := h.orderService.UpdateStatus(ctx, event.OrderID(), status)
	if err != nil {
		logger.L().Error(
			"Unable change order status",
			zap.String("event", event.Name()),
			zap.Error(err),
		)

		return
	}
}
