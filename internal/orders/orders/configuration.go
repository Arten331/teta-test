package orders

import (
	"errors"
	"fmt"

	"shop/internal/common/events"
	orderEvents "shop/internal/common/events/order"
	"shop/internal/orders/domain/order"
	"shop/internal/orders/domain/users"
)

// OrderConfiguration is an alias for a function that will take in a pointer to an OrderService and modify it
type OrderConfiguration func(os *OrderService) error

var ErrRequestedProductNotFound = func(productID uint64) error {
	return errors.New(
		fmt.Sprint("Requested product not found: ", productID),
	)
}

func NewOrderService(cfgs ...OrderConfiguration) (OrderService, error) {
	var err error

	s := OrderService{}
	// Apply all Configurations passed in
	for _, cfg := range cfgs {
		err = cfg(&s)
		if err != nil {
			return OrderService{}, err
		}
	}

	if s.eventPublisher == nil {
		return s, errors.New("Order service: service without event publisher and kafka event handler")
	}

	// Подпишем наш сервис на события
	s.eventPublisher.Subscribe(NewOrderStatusChangedHandler(&s),
		orderEvents.ReservedInStorage{},        // Заказ зарезервирован
		orderEvents.ReserveInStorageRejected{}, // Отказ в резерве
		orderEvents.PaymentSuccess{},           // Заказ зарезервирован
		orderEvents.PaymentRejected{},          // Отказ в резерве
	)

	return s, nil
}

func WithOrderRepository(or order.Repository) OrderConfiguration {
	return func(os *OrderService) error {
		os.repositories.order = or
		return nil
	}
}

func WithUserRepository(ur users.Repository) OrderConfiguration {
	return func(os *OrderService) error {
		os.repositories.user = ur
		return nil
	}
}

func WithEventPublisher(p *events.EventPublisher) OrderConfiguration {
	return func(os *OrderService) error {
		os.eventPublisher = p
		return nil
	}
}
