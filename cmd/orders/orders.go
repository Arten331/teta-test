package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"shop/internal/orders/app"
	"shop/internal/orders/config"
	"shop/pkg/logger"
)

func main() {
	var err error

	ctx, stop := context.WithCancel(context.Background())

	cfg, err := config.Init()
	if err != nil {
		log.Fatalf("Error configuration load, %v\n", err)
	}

	defer stop()

	err = logger.SetupDefaultLogger(logger.Options{
		Level: cfg.Logger.Level,
		Debug: cfg.Logger.Debug,
	})
	if err != nil {
		log.Fatalf("Error init logger, %v\n", err)
	}

	application, err := app.Init(ctx, cfg)
	if err != nil {
		log.Fatalf("Error load application modulues, %v\n", err)
	}

	err = application.Run(ctx, stop)
	if err != nil {
		log.Fatalf("Error run application %v\n", err)
	}

	sig := make(chan os.Signal, 1)

	signal.Notify(sig, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)

	go func() {
		<-sig

		shutdownCtx, cancel := context.WithTimeout(ctx, 30*time.Second)

		go func() {
			<-shutdownCtx.Done()

			if shutdownCtx.Err() == context.DeadlineExceeded {
				log.Fatal("graceful shutdown timed out.. forcing exit.")
			}
		}()

		err := application.Shutdown(ctx)
		if err != nil {
			log.Fatalln(err)
		}

		stop()
		cancel()
	}()

	<-ctx.Done()
}
