package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"shop/pkg/logger"
	"syscall"
	"time"

	"shop/internal/wallet/app"
	"shop/internal/wallet/config"
)

func main() {
	var err error

	ctx, stop := context.WithCancel(context.Background())

	cfg, err := config.Init()
	if err != nil {
		log.Fatalf("Error configuration load, %v", err)
	}

	defer stop()

	err = logger.SetupDefaultLogger(logger.Options{
		Level: cfg.Logger.Level,
		Debug: cfg.Logger.Debug,
	})
	if err != nil {
		log.Fatalf("Error init logger, %v\n", err)
	}

	application, err := app.Init(ctx, cfg)
	if err != nil {
		log.Fatalf("Error load application modulues, %v", err)
	}

	err = application.Run(ctx, stop)
	if err != nil {
		log.Fatalf("Error run application %v", err)
	}

	sig := make(chan os.Signal, 1)

	signal.Notify(sig, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)

	go func() {
		<-sig

		shutdownCtx, cancel := context.WithTimeout(ctx, 30*time.Second)

		go func() {
			<-shutdownCtx.Done()

			if shutdownCtx.Err() == context.DeadlineExceeded {
				log.Fatal("graceful shutdown timed out.. forcing exit.")
			}
		}()

		stop()
		cancel()
	}()

	<-ctx.Done()
}
